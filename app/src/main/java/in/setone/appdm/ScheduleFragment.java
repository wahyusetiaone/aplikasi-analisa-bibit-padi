//package in.setone.appdm;
//
//import android.os.Bundle;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Toast;
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.annotation.StringRes;
//import androidx.fragment.app.Fragment;
//import androidx.lifecycle.Observer;
//import androidx.lifecycle.ViewModelProviders;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//import in.setone.valent.siswa.adapter.schedule.SchedulerAdapterListJadwal;
//import in.setone.valent.siswa.data.headergrid.DataHeader;
//import in.setone.valent.siswa.data.headergrid.model.ListofDataHeader;
//import in.setone.valent.siswa.data.schedule.model.Request;
//import in.setone.valent.siswa.data.schedule.model.ResponseResults;
//import in.setone.valent.siswa.ui.headergrid.HeaderGridHelper;
//import in.setone.valent.siswa.utilities.Preferrences;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class ScheduleFragment extends Fragment {
//
//    private ScheduleViewModel scheduleViewModel;
//
//    private List<ResponseResults> data = new ArrayList<>();
//
//    private DataHeader dataHeader;
//
//    private String TAG = "ScheduleFragment";
//
//    private HeaderGridHelper gridHelper;
//
//    private RecyclerView recyclerView;
//
//    private RecyclerView.Adapter mAdapter;
//
//    private RecyclerView.LayoutManager layoutManager;
//
//    public View onCreateView(@NonNull LayoutInflater inflater,
//                             ViewGroup container, Bundle savedInstanceState) {
//        Log.d(TAG, "onCreateView");
//        scheduleViewModel =ViewModelProviders.of(this, new ScheduleModelFactory(getActivity().getApplicationContext()))
//                .get(ScheduleViewModel.class);
//        View root = inflater.inflate(R.layout.fragment_schedule, container, false);
//
//        gridHelper = HeaderGridHelper.getInstance(getContext(),root);
//
//        recyclerView = root.findViewById(R.id.list_view_jadwal);
//        layoutManager = new LinearLayoutManager(getContext());
//        recyclerView.setLayoutManager(layoutManager);
//
//        return root;
//    }
//
//    @Override
//    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//        final List<ListofDataHeader> list = new ArrayList<>();
//        list.add(new ListofDataHeader("Mata Pelajaran", 6));
//        list.add(new ListofDataHeader("Paket", 23));
//        dataHeader = new DataHeader(list);
//        Log.d(TAG, "onViewCreated");
//        dataHeader.getDataHeader().observe(requireActivity(), new Observer<List<ListofDataHeader>>() {
//            @Override
//            public void onChanged(List<ListofDataHeader> dataHeaders) {
//                Log.d(TAG, "onViewCreated - onChange Data Gird");
//                gridHelper.dataChangeGridHeader(dataHeaders);
//            }
//        });
//
//        scheduleViewModel.jadwal(new Request(Preferrences.getRegisteredId(getContext()))).observe(requireActivity(), new Observer<ScheduleResult>() {
//            @Override
//            public void onChanged(ScheduleResult scheduleResult) {
//                if (scheduleResult == null) {
//                    return;
//                }
//                if (scheduleResult.getError() != null){
//                    showLoginFailed(scheduleResult.getError());
//                }
//                if (scheduleResult.getSuccess() != null){
//                    if (data != null){
//                        data.clear();
//                    }
//                    assert ((ScheduleResult) scheduleResult).getSuccess() != null;
//                    data.addAll((List<ResponseResults>) ((ScheduleResult) scheduleResult).getSuccess().getData());
//                }
//                if (data != null){
//                    setupList();
//                    mAdapter.notifyDataSetChanged();
//                }
//            }
//        });
//
//    }
//
//    private void setupList() {
//        // specify an adapter (see also next example)
//        mAdapter = new SchedulerAdapterListJadwal(data);
//        recyclerView.setAdapter(mAdapter);
//    }
//
//    private void showLoginFailed(@StringRes Integer errorString) {
//        Toast.makeText(requireActivity().getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
//    }
//}