package in.setone.appdm.database.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tb_kriteria")
public class KriteriaData {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "_id")
    protected int id;

    @Nullable
    @ColumnInfo(name = "kode_variable")
    protected int kodevariabel;

    @Nullable
    @ColumnInfo(name = "kode_himpunan")
    protected int kodehimpunan;

    public KriteriaData(int id, int kodevariabel, int kodehimpunan) {
        this.id = id;
        this.kodevariabel = kodevariabel;
        this.kodehimpunan = kodehimpunan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getKodevariabel() {
        return kodevariabel;
    }

    public void setKodevariabel(int kodevariabel) {
        this.kodevariabel = kodevariabel;
    }

    public int getKodehimpunan() {
        return kodehimpunan;
    }

    public void setKodehimpunan(int kodehimpunan) {
        this.kodehimpunan = kodehimpunan;
    }

    @Override
    public String toString() {
        return "KriteriaData{" +
                "id=" + id +
                ", kodevariabel=" + kodevariabel +
                ", kodehimpunan=" + kodehimpunan +
                '}';
    }
}
