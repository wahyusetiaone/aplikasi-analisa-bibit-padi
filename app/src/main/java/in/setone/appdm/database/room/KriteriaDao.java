package in.setone.appdm.database.room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import in.setone.appdm.database.model.KriteriaData;
import in.setone.appdm.database.model.PadiData;

import java.util.List;

@Dao
public interface KriteriaDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(KriteriaData kriteriaData);

    @Query("SELECT * from tb_kriteria")
    LiveData<List<KriteriaData>> getKriteria();

    @Query("DELETE FROM tb_kriteria")
    void deleteKriteria();

    @Query("SELECT * from tb_kriteria WHERE _id = :id")
    LiveData<KriteriaData> showKriteria(int id);

    @Query("UPDATE tb_kriteria set kode_variable = :kodevariable, kode_himpunan = :kodehimpunan WHERE _id = :id")
    void updateKriteria(int id, int kodevariable, int kodehimpunan);

    @Query("DELETE FROM tb_kriteria WHERE _id = :id")
    void removeKriteria(int id);
//
//    @Delete
//    void delete(MoviesModel moviesModel);
//
//    @Query("SELECT * from movies_fav ORDER BY id ASC")
//    LiveData<List<Movies>> getResults();
//
//    //for widget
//    @Query("SELECT * from movies_fav ORDER BY id ASC")
//    List<Movies> getListMovies();
//
//    //for contentprovider
//    @Query("SELECT * from movies_fav ORDER BY id ASC")
//    Cursor getCursorMovies();
//
//    @Query("SELECT * from movies_fav WHERE id = :id")
//    Cursor loadCursorMovie(long id);

}
