package in.setone.appdm.database.room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import in.setone.appdm.database.model.KriteriaData;
import in.setone.appdm.database.model.PadiData;
import in.setone.appdm.database.model.TranningData;

import java.util.List;

@Dao
public interface TranningDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(TranningData tranningData);

    @Query("SELECT * from tb_tranning")
    LiveData<List<TranningData>> getTranning();

    @Query("DELETE FROM tb_tranning")
    void deleteTranning();

    @Query("SELECT * from tb_tranning WHERE _id = :id")
    LiveData<TranningData> showTranning(int id);

    @Query("UPDATE tb_tranning set kode_padi = :kode_padi,s_kode_kriteria = :skodekriteria WHERE _id = :id")
    void updateTranning(int id, int kode_padi, String skodekriteria);

    @Query("DELETE FROM tb_tranning WHERE _id = :id")
    void removeTranning(int id);

    @Query("SELECT COUNT(_id) FROM tb_tranning")
    LiveData<Integer> countRecord();
//
//    @Delete
//    void delete(MoviesModel moviesModel);
//
//    @Query("SELECT * from movies_fav ORDER BY id ASC")
//    LiveData<List<Movies>> getResults();
//
//    //for widget
//    @Query("SELECT * from movies_fav ORDER BY id ASC")
//    List<Movies> getListMovies();
//
//    //for contentprovider
//    @Query("SELECT * from movies_fav ORDER BY id ASC")
//    Cursor getCursorMovies();
//
//    @Query("SELECT * from movies_fav WHERE id = :id")
//    Cursor loadCursorMovie(long id);

}
