package in.setone.appdm.database.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tb_variable")
public class VariableData {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "_id")
    protected int id;

    @Nullable
    @ColumnInfo(name = "nama_variable")
    protected String nama;

    public VariableData(int id, @Nullable String nama) {
        this.id = id;
        this.nama = nama;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Nullable
    public String getNama() {
        return nama;
    }

    public void setNama(@Nullable String nama) {
        this.nama = nama;
    }

    @Override
    public String toString() {
        return "PadiData{" +
                "id=" + id +
                ", nama='" + nama + '\'' +
                '}';
    }
}
