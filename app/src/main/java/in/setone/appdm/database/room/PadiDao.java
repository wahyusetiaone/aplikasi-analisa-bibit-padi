package in.setone.appdm.database.room;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import in.setone.appdm.database.model.PadiData;

import java.util.List;

@Dao
public interface PadiDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(PadiData padiData);

    @Query("SELECT * from tb_padi")
    LiveData<List<PadiData>> getPadi();

    @Query("DELETE FROM tb_padi")
    void deletePadi();

    @Query("SELECT * from tb_padi WHERE _id = :id")
    LiveData<PadiData> showPadi(int id);

    @Query("UPDATE tb_padi set nama = :nama WHERE _id = :id")
    void updatePadi(int id, String nama);

    @Query("DELETE FROM tb_padi WHERE _id = :id")
    void removePadi(int id);

//
//    @Delete
//    void delete(MoviesModel moviesModel);
//
//    @Query("SELECT * from movies_fav ORDER BY id ASC")
//    LiveData<List<Movies>> getResults();
//
//    //for widget
//    @Query("SELECT * from movies_fav ORDER BY id ASC")
//    List<Movies> getListMovies();
//
//    //for contentprovider
//    @Query("SELECT * from movies_fav ORDER BY id ASC")
//    Cursor getCursorMovies();
//
//    @Query("SELECT * from movies_fav WHERE id = :id")
//    Cursor loadCursorMovie(long id);

}
