package in.setone.appdm.database.room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import in.setone.appdm.database.model.HimpunanData;
import in.setone.appdm.database.model.VariableData;

import java.util.List;

@Dao
public interface HimpunanDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(HimpunanData himpunanData);

    @Query("SELECT * from tb_himpunan")
    LiveData<List<HimpunanData>> getHimpunan();

    @Query("DELETE FROM tb_himpunan")
    void deleteHimpunan();

    @Query("SELECT * from tb_himpunan WHERE _id = :id")
    LiveData<HimpunanData> showHimpunan(int id);

    @Query("UPDATE tb_himpunan set nama_himpunan = :nama_himpunan WHERE _id = :id")
    void updateHimpunan(int id, int nama_himpunan);

    @Query("DELETE FROM tb_himpunan WHERE _id = :id")
    void removeHimpunan(int id);
//
//    @Delete
//    void delete(MoviesModel moviesModel);
//
//    @Query("SELECT * from movies_fav ORDER BY id ASC")
//    LiveData<List<Movies>> getResults();
//
//    //for widget
//    @Query("SELECT * from movies_fav ORDER BY id ASC")
//    List<Movies> getListMovies();
//
//    //for contentprovider
//    @Query("SELECT * from movies_fav ORDER BY id ASC")
//    Cursor getCursorMovies();
//
//    @Query("SELECT * from movies_fav WHERE id = :id")
//    Cursor loadCursorMovie(long id);

}
