package in.setone.appdm.database;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.RoomWarnings;
import androidx.sqlite.db.SupportSQLiteDatabase;
import in.setone.appdm.database.model.*;
import in.setone.appdm.database.room.*;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {PadiData.class, TranningData.class, KriteriaData.class, VariableData.class, HimpunanData.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract TranningDao tranningDao();
    public abstract PadiDao padiDao();
    public abstract KriteriaDao kriteriaDao();
    public abstract VariableDao variableDao();
    public abstract HimpunanDao himpunanDao();

    // marking the instance as volatile to ensure atomic access to the variable
    private static volatile AppDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;

    public static String DBNAME_CONTRACT = "db_bayes_padi";

    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    @SuppressWarnings(RoomWarnings.CANNOT_CREATE_VERIFICATION_DATABASE)
    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context,
                            AppDatabase.class, DBNAME_CONTRACT)
                            .setJournalMode(JournalMode.AUTOMATIC)
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    /**
     * Override the onOpen method to populate the database.
     * For this sample, we clear the database every time it is created or opened.
     *
     * If you want to populate the database only when the database is created for the 1st time,
     * override RoomDatabase.Callback()#onCreate
     */
    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db){
            super.onCreate(db);

            // If you want to keep data through app restarts,
            // comment out the following block
            databaseWriteExecutor.execute(() -> {
                // Populate the database in the background.
                // If you want to start with more words, just add them.
                defaultPadi();
                defaultVariable();
                defaultHimpunan();
                defaultKriteria();
                defaultTranning();
            });
        }

        private void defaultTranning() {
            TranningDao tranningDao = INSTANCE.tranningDao();
            tranningDao.insert(new TranningData(10001,101,"5201301,5202303,5203303,5204303"));
            tranningDao.insert(new TranningData(10002,102,"5201301,5202303,5203303,5204304"));
            tranningDao.insert(new TranningData(10003,103,"5201301,5202303,5203303,5204304"));
            tranningDao.insert(new TranningData(10004,104,"5201302,5202304,5203304,5204303"));
            tranningDao.insert(new TranningData(10005,105,"5201302,5202304,5203304,5204304"));
            tranningDao.insert(new TranningData(10006,106,"5201302,5202304,5203303,5204304"));
            tranningDao.insert(new TranningData(10007,107,"5201302,5202304,5203303,5204304"));
            tranningDao.insert(new TranningData(10008,101,"5201301,5202304,5203304,5204303"));
            tranningDao.insert(new TranningData(10009,102,"5201301,5202304,5203303,5204304"));
            tranningDao.insert(new TranningData(10010,103,"5201301,5202304,5203303,5204304"));
            tranningDao.insert(new TranningData(10011,104,"5201302,5202303,5203304,5204304"));
            tranningDao.insert(new TranningData(10012,105,"5201302,5202303,5203304,5204304"));
            tranningDao.insert(new TranningData(10013,106,"5201302,5202303,5203303,5204304"));
            tranningDao.insert(new TranningData(10014,107,"5201302,5202303,5203303,5204304"));
            tranningDao.insert(new TranningData(10015,101,"5201302,5202303,5203304,5204304"));
            tranningDao.insert(new TranningData(10016,102,"5201302,5202303,5203304,5204303"));
            tranningDao.insert(new TranningData(10017,103,"5201302,5202303,5203304,5204303"));
            tranningDao.insert(new TranningData(10018,104,"5201301,5202304,5203303,5204304"));
            tranningDao.insert(new TranningData(10019,105,"5201301,5202303,5203303,5204303"));
            tranningDao.insert(new TranningData(10020,106,"5201301,5202304,5203304,5204303"));
            tranningDao.insert(new TranningData(10021,107,"5201301,5202304,5203304,5204303"));
        }

        private void defaultKriteria() {
            KriteriaDao kriteriaDao = INSTANCE.kriteriaDao();
            kriteriaDao.insert(new KriteriaData(5201301,201,301));
            kriteriaDao.insert(new KriteriaData(5201302,201,302));
            kriteriaDao.insert(new KriteriaData(5202303,202,303));
            kriteriaDao.insert(new KriteriaData(5202304,202,304));
            kriteriaDao.insert(new KriteriaData(5203303,203,303));
            kriteriaDao.insert(new KriteriaData(5203304,203,304));
            kriteriaDao.insert(new KriteriaData(5204303,204,303));
            kriteriaDao.insert(new KriteriaData(5204304,204,304));
        }

        private void defaultHimpunan() {
            HimpunanDao himpunanDao = INSTANCE.himpunanDao();
            himpunanDao.insert(new HimpunanData(301,"Panas"));
            himpunanDao.insert(new HimpunanData(302,"Dingin"));
            himpunanDao.insert(new HimpunanData(303,"Rendah"));
            himpunanDao.insert(new HimpunanData(304,"Tinggi"));
        }

        private void defaultVariable() {
            VariableDao variableDao = INSTANCE.variableDao();
            variableDao.insert(new VariableData(201, "Cuaca"));
            variableDao.insert(new VariableData(202, "Produksi"));
            variableDao.insert(new VariableData(203, "Serangan Hama"));
            variableDao.insert(new VariableData(204, "Hasil Panen"));
        }

        private void defaultPadi() {
            PadiDao padiDao = INSTANCE.padiDao();
            padiDao.insert(new PadiData(101, "Mira"));
            padiDao.insert(new PadiData(102, "Ciherang"));
            padiDao.insert(new PadiData(103, "Mikongga"));
            padiDao.insert(new PadiData(104, "Ciibogo"));
            padiDao.insert(new PadiData(105, "Inpari 43 GSR"));
            padiDao.insert(new PadiData(106, "Mapan P-05"));
            padiDao.insert(new PadiData(107, "Sertani 13"));
        }

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);

        }
    };
}
