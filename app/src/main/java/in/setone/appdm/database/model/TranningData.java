package in.setone.appdm.database.model;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tb_tranning")
public class TranningData {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "_id")
    protected int id;

    @Nullable
    @ColumnInfo(name = "kode_padi")
    protected int kodepadi;

    @Nullable
    @ColumnInfo(name = "s_kode_kriteria")
    protected String skodekriteria;

    public TranningData(int id, int kodepadi, @Nullable String skodekriteria) {
        this.id = id;
        this.kodepadi = kodepadi;
        this.skodekriteria = skodekriteria;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getKodepadi() {
        return kodepadi;
    }

    public void setKodepadi(int kodepadi) {
        this.kodepadi = kodepadi;
    }

    @Nullable
    public String getSkodekriteria() {
        return skodekriteria;
    }

    public void setSkodekriteria(@Nullable String skodekriteria) {
        this.skodekriteria = skodekriteria;
    }

    @Override
    public String toString() {
        return "TranningData{" +
                "id=" + id +
                ", kodepadi=" + kodepadi +
                ", skodekriteria='" + skodekriteria + '\'' +
                '}';
    }
}
