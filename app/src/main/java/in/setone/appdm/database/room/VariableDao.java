package in.setone.appdm.database.room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import in.setone.appdm.database.model.KriteriaData;
import in.setone.appdm.database.model.PadiData;
import in.setone.appdm.database.model.VariableData;

import java.util.List;

@Dao
public interface VariableDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(VariableData variableData);

    @Query("SELECT * from tb_variable")
    LiveData<List<VariableData>> getVariable();

    @Query("DELETE FROM tb_variable")
    void deleteVariable();

    @Query("SELECT * from tb_variable WHERE _id = :id")
    LiveData<VariableData> showVariable(int id);

    @Query("UPDATE tb_variable set nama_variable = :nama_variable WHERE _id = :id")
    void updateKriteria(int id, int nama_variable);

    @Query("DELETE FROM tb_variable WHERE _id = :id")
    void removeVariable(int id);
//
//    @Delete
//    void delete(MoviesModel moviesModel);
//
//    @Query("SELECT * from movies_fav ORDER BY id ASC")
//    LiveData<List<Movies>> getResults();
//
//    //for widget
//    @Query("SELECT * from movies_fav ORDER BY id ASC")
//    List<Movies> getListMovies();
//
//    //for contentprovider
//    @Query("SELECT * from movies_fav ORDER BY id ASC")
//    Cursor getCursorMovies();
//
//    @Query("SELECT * from movies_fav WHERE id = :id")
//    Cursor loadCursorMovie(long id);

}
