package in.setone.appdm.pacel;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import in.setone.appdm.database.model.VariableData;

public class VariableParcelable extends VariableData implements Parcelable {
    public VariableParcelable(int id, @Nullable String nama) {
        super(id, nama);
    }

    protected VariableParcelable(Parcel in) {
        super(in.readInt(), in.readString());
    }

    public static final Creator<VariableParcelable> CREATOR = new Creator<VariableParcelable>() {
        @Override
        public VariableParcelable createFromParcel(Parcel in) {
            return new VariableParcelable(in);
        }

        @Override
        public VariableParcelable[] newArray(int size) {
            return new VariableParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeString(this.nama);
    }
}
