package in.setone.appdm.pacel;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import in.setone.appdm.database.model.PadiData;

public class PadiParcelable extends PadiData implements Parcelable {
    public PadiParcelable(int id, @Nullable String nama) {
        super(id, nama);
    }

    protected PadiParcelable(Parcel in) {
        super(in.readInt(),in.readString());
    }

    public static final Creator<PadiParcelable> CREATOR = new Creator<PadiParcelable>() {
        @Override
        public PadiParcelable createFromParcel(Parcel in) {
            return new PadiParcelable(in);
        }

        @Override
        public PadiParcelable[] newArray(int size) {
            return new PadiParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeString(this.nama);
    }
}
