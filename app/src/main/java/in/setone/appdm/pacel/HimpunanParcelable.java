package in.setone.appdm.pacel;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import in.setone.appdm.database.model.HimpunanData;

public class HimpunanParcelable extends HimpunanData implements Parcelable {
    public HimpunanParcelable(int id, @Nullable String nama) {
        super(id, nama);
    }

    protected HimpunanParcelable(Parcel in) {
        super(in.readInt(),in.readString());
    }

    public static final Creator<HimpunanParcelable> CREATOR = new Creator<HimpunanParcelable>() {
        @Override
        public HimpunanParcelable createFromParcel(Parcel in) {
            return new HimpunanParcelable(in);
        }

        @Override
        public HimpunanParcelable[] newArray(int size) {
            return new HimpunanParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeString(this.nama);
    }
}
