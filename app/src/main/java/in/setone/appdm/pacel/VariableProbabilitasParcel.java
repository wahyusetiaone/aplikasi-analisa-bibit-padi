package in.setone.appdm.pacel;

import android.os.Parcel;
import android.os.Parcelable;
import in.setone.appdm.data.newtranning.ProbabilitasItem;

import java.util.ArrayList;
import java.util.List;

public class VariableProbabilitasParcel implements Parcelable {
    private List<VariableProbabilitas> list = new ArrayList<>();
    private ClassLoader classLoader;
    public VariableProbabilitasParcel(List<VariableProbabilitas> list) {
        this.list = list;
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }

    public void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    public List<VariableProbabilitas> getList() {
        return list;
    }

    public void setList(List<VariableProbabilitas> list) {
        this.list = list;
    }

    public static Creator<VariableProbabilitasParcel> getCREATOR() {
        return CREATOR;
    }

    protected VariableProbabilitasParcel(Parcel in) {
        List<VariableProbabilitas> myList = new ArrayList<>();

        in.readList(myList, classLoader);
        setList(myList);
    }

    public static final Creator<VariableProbabilitasParcel> CREATOR = new Creator<VariableProbabilitasParcel>() {
        @Override
        public VariableProbabilitasParcel createFromParcel(Parcel in) {
            return new VariableProbabilitasParcel(in);
        }

        @Override
        public VariableProbabilitasParcel[] newArray(int size) {
            return new VariableProbabilitasParcel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(this.list);
    }

    @Override
    public String toString() {
        return "ProbabilitasParcel{" +
                "list=" + list +
                '}';
    }
}
