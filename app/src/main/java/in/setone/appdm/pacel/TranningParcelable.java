package in.setone.appdm.pacel;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import in.setone.appdm.database.model.TranningData;

public class TranningParcelable extends TranningData implements Parcelable {
    public TranningParcelable(int id, int kodepadi, @Nullable String skodekriteria) {
        super(id, kodepadi, skodekriteria);
    }

    protected TranningParcelable(Parcel in) {
        super(in.readInt(),in.readInt(),in.readString());
    }

    public static final Creator<TranningParcelable> CREATOR = new Creator<TranningParcelable>() {
        @Override
        public TranningParcelable createFromParcel(Parcel in) {
            return new TranningParcelable(in);
        }

        @Override
        public TranningParcelable[] newArray(int size) {
            return new TranningParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeInt(this.kodepadi);
        parcel.writeString(this.skodekriteria);
    }
}
