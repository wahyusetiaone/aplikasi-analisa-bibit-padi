package in.setone.appdm.pacel;

import android.os.Parcel;
import android.os.Parcelable;
import in.setone.appdm.data.home.TranningDataModel;
import in.setone.appdm.data.newtranning.ProbabilitasItem;

import java.util.ArrayList;
import java.util.List;

public class ProbabilitasParcel implements Parcelable {
    private List<ProbabilitasItem> list = new ArrayList<>();
    private ClassLoader classLoader;
    public ProbabilitasParcel(List<ProbabilitasItem> list) {
        this.list = list;
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }

    public void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    public List<ProbabilitasItem> getList() {
        return list;
    }

    public void setList(List<ProbabilitasItem> list) {
        this.list = list;
    }

    public static Creator<ProbabilitasParcel> getCREATOR() {
        return CREATOR;
    }

    protected ProbabilitasParcel(Parcel in) {
        List<ProbabilitasItem> myList = new ArrayList<>();

        in.readList(myList, classLoader);
        setList(myList);
    }

    public static final Creator<ProbabilitasParcel> CREATOR = new Creator<ProbabilitasParcel>() {
        @Override
        public ProbabilitasParcel createFromParcel(Parcel in) {
            return new ProbabilitasParcel(in);
        }

        @Override
        public ProbabilitasParcel[] newArray(int size) {
            return new ProbabilitasParcel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(this.list);
    }

    @Override
    public String toString() {
        return "ProbabilitasParcel{" +
                "list=" + list +
                '}';
    }
}
