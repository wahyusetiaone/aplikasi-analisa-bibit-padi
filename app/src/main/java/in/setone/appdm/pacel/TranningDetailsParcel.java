package in.setone.appdm.pacel;

import android.os.Parcel;
import android.os.Parcelable;
import in.setone.appdm.data.home.TranningDataModel;
import in.setone.appdm.database.model.TranningData;
import in.setone.appdm.ui.newtranning.FormDataTranningActivity;

import java.util.ArrayList;
import java.util.List;

public class TranningDetailsParcel implements Parcelable {
    private List<TranningDataModel> list = new ArrayList<>();
    private ClassLoader classLoader;
    public TranningDetailsParcel(List<TranningDataModel> list) {
        this.list = list;
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }

    public void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    public List<TranningDataModel> getList() {
        return list;
    }

    public void setList(List<TranningDataModel> list) {
        this.list = list;
    }

    public static Creator<TranningDetailsParcel> getCREATOR() {
        return CREATOR;
    }

    protected TranningDetailsParcel(Parcel in) {
        List<TranningDataModel> myList = new ArrayList<>();

        in.readList(myList, classLoader);
        setList(myList);
    }

    public static final Creator<TranningDetailsParcel> CREATOR = new Creator<TranningDetailsParcel>() {
        @Override
        public TranningDetailsParcel createFromParcel(Parcel in) {
            return new TranningDetailsParcel(in);
        }

        @Override
        public TranningDetailsParcel[] newArray(int size) {
            return new TranningDetailsParcel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(this.list);
    }

    @Override
    public String toString() {
        return "TranningDetailsParcel{" +
                "list=" + list +
                '}';
    }
}
