package in.setone.appdm.pacel;

import android.os.Parcel;
import android.os.Parcelable;
import in.setone.appdm.database.model.KriteriaData;

public class KriteriaParcelable extends KriteriaData implements Parcelable {
    public KriteriaParcelable(int id, int kodevariabel, int kodehimpunan) {
        super(id, kodevariabel, kodehimpunan);
    }

    protected KriteriaParcelable(Parcel in) {
        super(in.readInt(),in.readInt(),in.readInt());
    }

    public static final Creator<KriteriaParcelable> CREATOR = new Creator<KriteriaParcelable>() {
        @Override
        public KriteriaParcelable createFromParcel(Parcel in) {
            return new KriteriaParcelable(in);
        }

        @Override
        public KriteriaParcelable[] newArray(int size) {
            return new KriteriaParcelable[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeInt(this.kodevariabel);
        parcel.writeInt(this.kodehimpunan);
    }
}
