package in.setone.appdm.pacel;

import android.os.Parcelable;

import java.io.Serializable;

public class VariableProbabilitas implements Serializable {
    private int variable;
    private int himpunan;

    public VariableProbabilitas(int variable, int himpunan) {
        this.variable = variable;
        this.himpunan = himpunan;
    }

    @Override
    public String toString() {
        return "VariableProbabilitas{" +
                "variable=" + variable +
                ", himpunan=" + himpunan +
                '}';
    }

    public int getVariable() {
        return variable;
    }

    public void setVariable(int variable) {
        this.variable = variable;
    }

    public int getHimpunan() {
        return himpunan;
    }

    public void setHimpunan(int himpunan) {
        this.himpunan = himpunan;
    }
}
