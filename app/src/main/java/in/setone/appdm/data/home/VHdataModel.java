package in.setone.appdm.data.home;

import java.io.Serializable;

public class VHdataModel implements Serializable {

    private int id;
    private int _variable;
    private String variable;
    private int _himpunan;
    private String himpunan;

    public VHdataModel(int id, int _variable, String variable, int _himpunan, String himpunan) {
        this.id = id;
        this._variable = _variable;
        this.variable = variable;
        this._himpunan = _himpunan;
        this.himpunan = himpunan;
    }

    @Override
    public String toString() {
        return "VHdataModel{" +
                "id=" + id +
                ", _variable=" + _variable +
                ", variable='" + variable + '\'' +
                ", _himpunan=" + _himpunan +
                ", himpunan='" + himpunan + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int get_variable() {
        return _variable;
    }

    public void set_variable(int _variable) {
        this._variable = _variable;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public int get_himpunan() {
        return _himpunan;
    }

    public void set_himpunan(int _himpunan) {
        this._himpunan = _himpunan;
    }

    public String getHimpunan() {
        return himpunan;
    }

    public void setHimpunan(String himpunan) {
        this.himpunan = himpunan;
    }
}
