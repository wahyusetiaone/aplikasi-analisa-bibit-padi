package in.setone.appdm.data.newtranning;

import android.os.AsyncTask;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import in.setone.appdm.data.Result;
import in.setone.appdm.data.home.HomeDatasource;
import in.setone.appdm.data.home.HomeRepository;
import in.setone.appdm.database.AppDatabase;
import in.setone.appdm.database.model.*;
import in.setone.appdm.database.room.*;

import java.util.List;

public class TranningDatasource {

    public interface CallBackPadiDatasource {
        void onCallpadi(Result<List<PadiData>> result);
    }

    public interface CallBackVariableDatasource {
        void onCallVariable(Result<List<VariableData>> result);
    }

    public interface CallBackHimpunanDatasource {
        void onCallHimpunan(Result<List<HimpunanData>> result);
    }

    public interface CallBackKriteriaDatasource {
        void onCallKriteria(Result<List<KriteriaData>> result);
    }

    public void getPadi(LifecycleOwner owner, PadiDao padiDao, AppDatabase appDatabase, final TranningDatasource.CallBackPadiDatasource backHomeDatasource) {
        padiDao = appDatabase.padiDao();
        padiDao.getPadi().observe(owner, new Observer<List<PadiData>>() {
            @Override
            public void onChanged(List<PadiData> padiData) {
                if (padiData.size() != 0) {
                    backHomeDatasource.onCallpadi(new Result.Success<>(padiData));
                }
                backHomeDatasource.onCallpadi(new Result.Error(new Throwable("Tidak ada record pada Data Variable")));

            }
        });
    }

    public void getVariable(LifecycleOwner owner, VariableDao variableDao, AppDatabase appDatabase, final TranningDatasource.CallBackVariableDatasource backHomeDatasource) {
        variableDao = appDatabase.variableDao();
        variableDao.getVariable().observe(owner, new Observer<List<VariableData>>() {
            @Override
            public void onChanged(List<VariableData> variableData) {
                if (variableData.size() != 0) {
                    backHomeDatasource.onCallVariable(new Result.Success<>(variableData));
                }
                backHomeDatasource.onCallVariable(new Result.Error(new Throwable("Tidak ada record pada Data Variable")));

            }
        });
    }

    public void getHimpunan(LifecycleOwner owner, HimpunanDao himpunanDao, AppDatabase appDatabase, final TranningDatasource.CallBackHimpunanDatasource backHomeDatasource) {
        himpunanDao = appDatabase.himpunanDao();
        himpunanDao.getHimpunan().observe(owner, new Observer<List<HimpunanData>>() {
            @Override
            public void onChanged(List<HimpunanData> himpunanData) {
                if (himpunanData.size() != 0) {
                    backHomeDatasource.onCallHimpunan(new Result.Success<>(himpunanData));
                }
                backHomeDatasource.onCallHimpunan(new Result.Error(new Throwable("Tidak ada record pada Data Himpunan")));

            }
        });
    }

    public void getKriteria(LifecycleOwner owner, KriteriaDao kriteriaDao, AppDatabase appDatabase, final TranningDatasource.CallBackKriteriaDatasource backHomeDatasource) {
        kriteriaDao = appDatabase.kriteriaDao();
        kriteriaDao.getKriteria().observe(owner, new Observer<List<KriteriaData>>() {
            @Override
            public void onChanged(List<KriteriaData> kriteriaData) {
                if (kriteriaData.size() != 0) {
                    backHomeDatasource.onCallKriteria(new Result.Success<>(kriteriaData));
                }
                backHomeDatasource.onCallKriteria(new Result.Error(new Throwable("Tidak ada record pada Data Kriteria")));

            }
        });
    }

    public LiveData<Integer> getLasted(TranningDao tranningDao, AppDatabase appDatabase){
        tranningDao = appDatabase.tranningDao();
        return tranningDao.countRecord();
    }

    public void insertTranning(TranningDao tranningDao, AppDatabase appDatabase, TranningData data){
        tranningDao = appDatabase.tranningDao();
        new insertAsyncTask(tranningDao).execute(data);
    };


    private static class insertAsyncTask extends AsyncTask<TranningData, Void, Void> {

        private TranningDao mAsyncTaskDao;

        insertAsyncTask(TranningDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final TranningData... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

}
