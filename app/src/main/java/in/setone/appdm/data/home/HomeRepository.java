package in.setone.appdm.data.home;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import in.setone.appdm.data.Result;
import in.setone.appdm.database.AppDatabase;
import in.setone.appdm.database.model.HimpunanData;
import in.setone.appdm.database.model.KriteriaData;
import in.setone.appdm.database.model.TranningData;
import in.setone.appdm.database.model.VariableData;
import in.setone.appdm.database.room.*;

import java.security.acl.Owner;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class HomeRepository {

    private static String _ERR_KEY = " Error Found in : ";
    private static volatile HomeRepository instance;
    private static String TAG = "HomeRepository";

    private List<TranningData> tranning = new ArrayList<>();
    private List<KriteriaData> kriteria = new ArrayList<>();
    private List<VariableData> variable = new ArrayList<>();
    private List<HimpunanData> himpunan = new ArrayList<>();

    public Boolean statusDataTranning;
    public Boolean statusDataKriteria;
    public Boolean statusDataVariable;
    public Boolean statusDataHimpunan;

    private AppDatabase appDatabase;
    private HomeDatasource dataSource;

    private PadiDao padiDao;
    private TranningDao tranningDao;
    private KriteriaDao kriteriaDao;
    private VariableDao variableDao;
    private HimpunanDao himpunanDao;

    private MutableLiveData<HomeModel> homeModel = new MutableLiveData<>();

    private String _err = "";

    // If user credentials will be cached in local storage, it is recommended it be encrypted
    // @see https://developer.android.com/training/articles/keystore

    // private constructor : singleton access
    private HomeRepository(HomeDatasource dataSource, AppDatabase appDatabase) {
        this.dataSource = dataSource;
        this.appDatabase = appDatabase;
        appDatabase.getOpenHelper().getWritableDatabase();
        padiDao = appDatabase.padiDao();
        tranningDao = appDatabase.tranningDao();
        kriteriaDao = appDatabase.kriteriaDao();
        variableDao = appDatabase.variableDao();
        himpunanDao = appDatabase.himpunanDao();
    }

    public static HomeRepository getInstance(HomeDatasource dataSource, AppDatabase appDatabase) {
        if (instance == null) {
            instance = new HomeRepository(dataSource, appDatabase);
        }
        return instance;
    }
    public interface CallData{
        void onResult(Result<MutableLiveData<HomeModel>> result);
    }

    public void datapadi(final LifecycleOwner owner, final  HomeDatasource.CallBackPadiDatasource datasource){
        dataSource.getPadi(owner, padiDao, appDatabase,datasource);
    }

    public void dataTranning(final LifecycleOwner owner, final  HomeDatasource.CallBackTranningDatasource datasource){
        dataSource.getTranining(owner, tranningDao, appDatabase,datasource);
    }

    public void dataKriteria(final LifecycleOwner owner, final  HomeDatasource.CallBackKriteriaDatasource datasource){
        dataSource.getKriteria(owner, kriteriaDao, appDatabase,datasource);
    }

    public void datavariable(final LifecycleOwner owner, final  HomeDatasource.CallBackVariableDatasource datasource){
        dataSource.getVariable(owner, variableDao, appDatabase,datasource);
    }

    public void dataHimpunan(final LifecycleOwner owner, final  HomeDatasource.CallBackHimpunanDatasource datasource){
        dataSource.getHimpunan(owner, himpunanDao, appDatabase,datasource);
    }

    //himpunan coba
    public void insert(HimpunanData table) {
        new insertAsyncTask(himpunanDao).execute(table);
    }

    private static class insertAsyncTask extends AsyncTask<HimpunanData, Void, Void> {

        private HimpunanDao mAsyncTaskDao;

        insertAsyncTask(HimpunanDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final HimpunanData... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

}
