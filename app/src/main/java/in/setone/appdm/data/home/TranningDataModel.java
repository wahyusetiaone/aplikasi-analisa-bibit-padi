package in.setone.appdm.data.home;

import java.io.Serializable;
import java.util.List;

public class TranningDataModel implements Serializable {
    private int id;
    private String name;
    private List<VHdataModel> varhim;

    public TranningDataModel(int id, String name, List<VHdataModel> varhim) {
        this.id = id;
        this.name = name;
        this.varhim = varhim;
    }

    @Override
    public String toString() {
        return "TranningDataModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", varhim=" + varhim +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<VHdataModel> getVarhim() {
        return varhim;
    }

    public void setVarhim(List<VHdataModel> varhim) {
        this.varhim = varhim;
    }
}
