package in.setone.appdm.data.newtranning;

import java.io.Serializable;
import java.util.List;

public class ProbabilitasItem implements Serializable {
    private int id;
    private String nama;
    private List<VariableModel> variable;

    public ProbabilitasItem(int id, String nama, List<VariableModel> variable) {
        this.id = id;
        this.nama = nama;
        this.variable = variable;
    }

    @Override
    public String toString() {
        return "ProbabilitasItem{" +
                "id=" + id +
                ", nama='" + nama + '\'' +
                ", variable=" + variable +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public List<VariableModel> getVariable() {
        return variable;
    }

    public void setVariable(List<VariableModel> variable) {
        this.variable = variable;
    }
}
