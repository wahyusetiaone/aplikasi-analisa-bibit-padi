package in.setone.appdm.data.newtranning;

import in.setone.appdm.database.model.*;

import java.util.List;

public class TranningModel {

    private List<PadiData> dataPadi;
    private List<VariableData> dataVariable;
    private List<HimpunanData> dataHimpunan;
    private List<KriteriaData> dataKriteria;

    public TranningModel(List<PadiData> dataPadi, List<VariableData> dataVariable, List<HimpunanData> dataHimpunan, List<KriteriaData> dataKriteria) {
        this.dataPadi = dataPadi;
        this.dataVariable = dataVariable;
        this.dataHimpunan = dataHimpunan;
        this.dataKriteria = dataKriteria;
    }

    public List<PadiData> getDataPadi() {
        return dataPadi;
    }

    public void setDataPadi(List<PadiData> dataPadi) {
        this.dataPadi = dataPadi;
    }

    public List<VariableData> getDataVariable() {
        return dataVariable;
    }

    public void setDataVariable(List<VariableData> dataVariable) {
        this.dataVariable = dataVariable;
    }

    public List<HimpunanData> getDataHimpunan() {
        return dataHimpunan;
    }

    public void setDataHimpunan(List<HimpunanData> dataHimpunan) {
        this.dataHimpunan = dataHimpunan;
    }

    public List<KriteriaData> getDataKriteria() {
        return dataKriteria;
    }

    public void setDataKriteria(List<KriteriaData> dataKriteria) {
        this.dataKriteria = dataKriteria;
    }

    @Override
    public String toString() {
        return "TranningModel{" +
                "dataPadi=" + dataPadi +
                ", dataVariable=" + dataVariable +
                ", dataHimpunan=" + dataHimpunan +
                ", dataKriteria=" + dataKriteria +
                '}';
    }
}
