package in.setone.appdm.data.home;

import androidx.lifecycle.LiveData;
import in.setone.appdm.database.model.*;

import java.util.List;

public class HomeModel {

    private List<PadiData> dataPadi;
    private List<TranningData> dataTranning;
    private List<KriteriaData> dataKriteria;
    private List<VariableData> dataVariable;
    private List<HimpunanData> dataHimpunan;

    public HomeModel(List<PadiData> dataPadi ,List<TranningData> dataTranning, List<KriteriaData> dataKriteria, List<VariableData> dataVariable, List<HimpunanData> dataHimpunan) {
        this.dataPadi = dataPadi;
        this.dataTranning = dataTranning;
        this.dataKriteria = dataKriteria;
        this.dataVariable = dataVariable;
        this.dataHimpunan = dataHimpunan;
    }

    public List<PadiData> getDataPadi() {
        return dataPadi;
    }

    public void setDataPadi(List<PadiData> dataPadi) {
        this.dataPadi = dataPadi;
    }

    public List<TranningData> getDataTranning() {
        return dataTranning;
    }

    public void setDataTranning(List<TranningData> dataTranning) {
        this.dataTranning = dataTranning;
    }

    public List<KriteriaData> getDataKriteria() {
        return dataKriteria;
    }

    public void setDataKriteria(List<KriteriaData> dataKriteria) {
        this.dataKriteria = dataKriteria;
    }

    public List<VariableData> getDataVariable() {
        return dataVariable;
    }

    public void setDataVariable(List<VariableData> dataVariable) {
        this.dataVariable = dataVariable;
    }

    public List<HimpunanData> getDataHimpunan() {
        return dataHimpunan;
    }

    public void setDataHimpunan(List<HimpunanData> dataHimpunan) {
        this.dataHimpunan = dataHimpunan;
    }

    @Override
    public String toString() {
        return "HomeModel{" +
                "dataPadi=" + dataPadi +
                ", dataTranning=" + dataTranning +
                ", dataKriteria=" + dataKriteria +
                ", dataVariable=" + dataVariable +
                ", dataHimpunan=" + dataHimpunan +
                '}';
    }
}
