package in.setone.appdm.data.home;

import android.app.Activity;
import android.app.ListActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import in.setone.appdm.data.Result;
import in.setone.appdm.database.AppDatabase;
import in.setone.appdm.database.model.*;
import in.setone.appdm.database.room.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HomeDatasource {

    public interface CallBackPadiDatasource {
        void onCallPadi(Result<List<PadiData>> result);
    }

    public interface CallBackTranningDatasource {
        void onCallTranning(Result<List<TranningData>> result);
    }

    public interface CallBackVariableDatasource {
        void onCallVariable(Result<List<VariableData>> result);
    }

    public interface CallBackKriteriaDatasource {
        void onCallKriteria(Result<List<KriteriaData>> result);
    }

    public interface CallBackHimpunanDatasource {
        void onCallHimpunan(Result<List<HimpunanData>> result);
    }

    public void getPadi(LifecycleOwner owner, PadiDao padiDao, AppDatabase appDatabase, final HomeDatasource.CallBackPadiDatasource backHomeDatasource) {

        padiDao = appDatabase.padiDao();
        padiDao.getPadi().observe(owner, new Observer<List<PadiData>>() {
            @Override
            public void onChanged(List<PadiData> padiData) {
                if (padiData.size() != 0) {
                    backHomeDatasource.onCallPadi(new Result.Success<>(padiData));
                }
                backHomeDatasource.onCallPadi(new Result.Error(new Throwable("Tidak ada record pada Data Tranning")));

            }
        });
    }

    public void getTranining(LifecycleOwner owner, TranningDao tranningDao, AppDatabase appDatabase, final HomeDatasource.CallBackTranningDatasource backHomeDatasource) {

        tranningDao = appDatabase.tranningDao();
        tranningDao.getTranning().observe(owner, new Observer<List<TranningData>>() {
            @Override
            public void onChanged(List<TranningData> tranningData) {
                if (tranningData.size() != 0) {
                    backHomeDatasource.onCallTranning(new Result.Success<>(tranningData));
                }
                backHomeDatasource.onCallTranning(new Result.Error(new Throwable("Tidak ada record pada Data Tranning")));

            }
        });
    }

    public void getKriteria(LifecycleOwner owner, KriteriaDao kriteriaDao, AppDatabase appDatabase, final HomeDatasource.CallBackKriteriaDatasource backHomeDatasource) {
        kriteriaDao = appDatabase.kriteriaDao();
        kriteriaDao.getKriteria().observe(owner, new Observer<List<KriteriaData>>() {
            @Override
            public void onChanged(List<KriteriaData> kriteriaData) {
                if (kriteriaData.size() != 0) {
                    backHomeDatasource.onCallKriteria(new Result.Success<>(kriteriaData));
                }
                backHomeDatasource.onCallKriteria(new Result.Error(new Throwable("Tidak ada record pada Data Kriteria")));

            }
        });
    }

    public void getVariable(LifecycleOwner owner, VariableDao variableDao, AppDatabase appDatabase, final HomeDatasource.CallBackVariableDatasource backHomeDatasource) {
        variableDao = appDatabase.variableDao();
        variableDao.getVariable().observe(owner, new Observer<List<VariableData>>() {
            @Override
            public void onChanged(List<VariableData> variableData) {
                if (variableData.size() != 0) {
                    backHomeDatasource.onCallVariable(new Result.Success<>(variableData));
                }
                backHomeDatasource.onCallVariable(new Result.Error(new Throwable("Tidak ada record pada Data Variable")));

            }
        });
    }

    public void getHimpunan(LifecycleOwner owner, HimpunanDao himpunanDao, AppDatabase appDatabase, final HomeDatasource.CallBackHimpunanDatasource backHomeDatasource) {
        himpunanDao = appDatabase.himpunanDao();
        himpunanDao.getHimpunan().observe(owner, new Observer<List<HimpunanData>>() {
            @Override
            public void onChanged(List<HimpunanData> himpunanData) {
                if (himpunanData.size() != 0) {
                    backHomeDatasource.onCallHimpunan(new Result.Success<>(himpunanData));
                }
                backHomeDatasource.onCallHimpunan(new Result.Error(new Throwable("Tidak ada record pada Data Himpunan")));

            }
        });
    }

//    public LiveData<List<TranningData>> getTranining(TranningDao tranningDao, AppDatabase appDatabase){
//        tranningDao = appDatabase.tranningDao();
//        return tranningDao.getTranning();
//    }
//
//    public LiveData<List<KriteriaData>> getKriteria(KriteriaDao kriteriaDao, AppDatabase appDatabase){
//        kriteriaDao = appDatabase.kriteriaDao();
//        return kriteriaDao.getKriteria();
//    }
//
//    public LiveData<List<VariableData>> getVariable(VariableDao variableDao, AppDatabase appDatabase){
//        variableDao = appDatabase.variableDao();
//        return variableDao.getVariable();
//    }
//
//    public LiveData<List<HimpunanData>> getHimpunan(HimpunanDao himpunanDao, AppDatabase appDatabase){
//        himpunanDao = appDatabase.himpunanDao();
//        return himpunanDao.getHimpunan();
//    }
}
