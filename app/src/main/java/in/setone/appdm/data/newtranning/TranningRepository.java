package in.setone.appdm.data.newtranning;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import in.setone.appdm.data.home.HomeDatasource;
import in.setone.appdm.data.home.HomeRepository;
import in.setone.appdm.database.AppDatabase;
import in.setone.appdm.database.model.TranningData;
import in.setone.appdm.database.room.*;

public class TranningRepository {

    private static volatile TranningRepository instance;

    private AppDatabase appDatabase;
    private TranningDatasource dataSource;

    private PadiDao padiDao;
    private TranningDao tranningDao;
    private VariableDao variableDao;
    private HimpunanDao himpunanDao;
    private KriteriaDao kriteriaDao;

    private TranningRepository(TranningDatasource dataSource, AppDatabase appDatabase) {
        this.dataSource = dataSource;
        this.appDatabase = appDatabase;
        appDatabase.getOpenHelper().getWritableDatabase();
        padiDao = appDatabase.padiDao();
        tranningDao = appDatabase.tranningDao();
        variableDao = appDatabase.variableDao();
        himpunanDao = appDatabase.himpunanDao();
        kriteriaDao = appDatabase.kriteriaDao();
    }

    public static TranningRepository getInstance(TranningDatasource dataSource, AppDatabase appDatabase) {
        if (instance == null) {
            instance = new TranningRepository(dataSource, appDatabase);
        }
        return instance;
    }

    public void dataPadi(final LifecycleOwner owner, final  TranningDatasource.CallBackPadiDatasource datasource){
        dataSource.getPadi(owner, padiDao, appDatabase,datasource);
    }

    public void dataVariable(final LifecycleOwner owner, final  TranningDatasource.CallBackVariableDatasource datasource){
        dataSource.getVariable(owner, variableDao, appDatabase,datasource);
    }

    public void dataHimpunan(final LifecycleOwner owner, final  TranningDatasource.CallBackHimpunanDatasource datasource){
        dataSource.getHimpunan(owner, himpunanDao, appDatabase,datasource);
    }

    public void dataKriteria(final LifecycleOwner owner, final TranningDatasource.CallBackKriteriaDatasource datasource){
        dataSource.getKriteria(owner, kriteriaDao, appDatabase, datasource);
    }

    public LiveData<Integer> getLastedID(){
        return dataSource.getLasted(tranningDao,appDatabase);
    }

    public void insertDataTranning(TranningData tranningData){
        dataSource.insertTranning(tranningDao, appDatabase, tranningData);
    }

}
