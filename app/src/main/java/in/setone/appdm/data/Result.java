package in.setone.appdm.data;

/**
 * A generic class that holds a result success w/ data or an error exception.
 */
public class Result<T> {
    // hide the private constructor to limit subclass types (Success, Error)
    private Result() {
    }

    @Override
    public String toString() {
        if (this instanceof in.setone.appdm.data.Result.Success) {
            in.setone.appdm.data.Result.Success success = (in.setone.appdm.data.Result.Success) this;
            return "Success[data=" + success.getData().toString() + "]";
        } else if (this instanceof in.setone.appdm.data.Result.Error) {
            in.setone.appdm.data.Result.Error error = (in.setone.appdm.data.Result.Error) this;
            return "Error[exception=" + error.getError().getMessage() + "]";
        }
        return "";
    }

    // Success sub-class
    public final static class Success<T> extends in.setone.appdm.data.Result {
        private T data;

        public Success(T data) {
            this.data = data;
        }

        public T getData() {
            return this.data;
        }
    }

    // Error sub-class
    public final static class Error extends in.setone.appdm.data.Result {
        private Throwable error;

        public Error(Throwable error) {
            this.error = error;
        }

        public Throwable getError() {
            return this.error;
        }
    }
}
