package in.setone.appdm.data.newtranning;

import java.io.Serializable;
import java.util.List;

public class VariableModel implements Serializable {
    private int id;
    private String name;
    private List<HimpunanModel> himpunan;

    public VariableModel(int id, String name, List<HimpunanModel> himpunan) {
        this.id = id;
        this.name = name;
        this.himpunan = himpunan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<HimpunanModel> getHimpunan() {
        return himpunan;
    }

    public void setHimpunan(List<HimpunanModel> himpunan) {
        this.himpunan = himpunan;
    }

    @Override
    public String toString() {
        return "VariableWithHimpunanModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", himpunan=" + himpunan +
                '}';
    }
}
