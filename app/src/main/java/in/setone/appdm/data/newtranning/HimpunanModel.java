package in.setone.appdm.data.newtranning;

import java.io.Serializable;

public class HimpunanModel implements Serializable {
    private int id;
    private String nama;
    private int jumlah;
    private int positif;
    private int negatif;

    public HimpunanModel(int id, String nama, int jumlah, int positif, int negatif) {
        this.id = id;
        this.nama = nama;
        this.jumlah = jumlah;
        this.positif = positif;
        this.negatif = negatif;
    }

    @Override
    public String toString() {
        return "HimpunanModel{" +
                "id=" + id +
                ", nama='" + nama + '\'' +
                ", jumlah=" + jumlah +
                ", positif=" + positif +
                ", negatif=" + negatif +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public int getPositif() {
        return positif;
    }

    public void setPositif(int positif) {
        this.positif = positif;
    }

    public int getNegatif() {
        return negatif;
    }

    public void setNegatif(int negatif) {
        this.negatif = negatif;
    }
}
