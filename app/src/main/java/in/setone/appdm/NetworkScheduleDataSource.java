//package in.setone.appdm;
//
//import android.util.Log;
//import in.setone.valent.siswa.data.Result;
//import in.setone.valent.siswa.data.schedule.model.CallbackResponse;
//import in.setone.valent.siswa.data.schedule.model.Request;
//import in.setone.valent.siswa.data.schedule.model.ResponseResults;
//import in.setone.valent.siswa.netwok.RetrofitService;
//import in.setone.valent.siswa.netwok.ServicesAPI;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//import java.util.List;
//
//public class NetworkScheduleDataSource {
//
//    private String TAG = "NetworkScheduleDataSource";
//
//    private ServicesAPI servicesAPI;
//
//    public interface NetworkScheduleDataSourceCallback {
//        void onScheduleRequestSuccess(Result<List<ResponseResults>> resultsResult);
//
//        void onScheduleRequestFailed(Result<List<ResponseResults>> resultsResult);
//    }
//
//    public void jadwal(final Request request, final NetworkScheduleDataSourceCallback sourceCallback){
//        servicesAPI = RetrofitService.createServiceApiToken(ServicesAPI.class);
//
//        servicesAPI.jadwalSiswa(request.getId_siswa()).enqueue(new Callback<CallbackResponse>() {
//            @Override
//            public void onResponse(Call<CallbackResponse> call, Response<CallbackResponse> response) {
//                if (response.body().isStatus()){
//                    Log.d(TAG,response.body().getResults().toString());
//                    sourceCallback.onScheduleRequestSuccess(new Result.Success<>(response.body().getResults()));
//                }
//                Log.i(TAG, response.body().getMessage());
//            }
//
//            @Override
//            public void onFailure(Call<CallbackResponse> call, Throwable t) {
//                sourceCallback.onScheduleRequestFailed(new Result.Error(t));
//                Log.e(TAG, t.getMessage());
//            }
//        });
//    }
//
//}
