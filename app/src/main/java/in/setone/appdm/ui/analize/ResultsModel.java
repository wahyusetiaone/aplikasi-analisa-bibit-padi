package in.setone.appdm.ui.analize;

import java.util.Comparator;

public class ResultsModel implements Comparable {
    private int nomor;
    private int kode_padi;
    private String nama_padi;
    private double probabilitas;
    private String kualitas;

    public ResultsModel(int nomor, int kode_padi, String nama_padi, double probabilitas, String kualitas) {
        this.nomor = nomor;
        this.kode_padi = kode_padi;
        this.nama_padi = nama_padi;
        this.probabilitas = probabilitas;
        this.kualitas = kualitas;
    }

    @Override
    public String toString() {
        return "ResultsModel{" +
                "nomor=" + nomor +
                ", kode_padi=" + kode_padi +
                ", nama_padi='" + nama_padi + '\'' +
                ", probabilitas=" + probabilitas +
                ", kualitas='" + kualitas + '\'' +
                '}';
    }

    public int getNomor() {
        return nomor;
    }

    public void setNomor(int nomor) {
        this.nomor = nomor;
    }

    public int getKode_padi() {
        return kode_padi;
    }

    public void setKode_padi(int kode_padi) {
        this.kode_padi = kode_padi;
    }

    public String getNama_padi() {
        return nama_padi;
    }

    public void setNama_padi(String nama_padi) {
        this.nama_padi = nama_padi;
    }

    public double getProbabilitas() {
        return probabilitas;
    }

    public void setProbabilitas(double probabilitas) {
        this.probabilitas = probabilitas;
    }

    public String getKualitas() {
        return kualitas;
    }

    public void setKualitas(String kualitas) {
        this.kualitas = kualitas;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
