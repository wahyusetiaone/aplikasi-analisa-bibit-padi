package in.setone.appdm.ui.newtranning;

import in.setone.appdm.pacel.VariableParcelable;

import java.util.List;

/**
 * Class exposing authenticated user details to the UI.
 */
class TranningUiResult {
    private List<VariableParcelable> data;
    //... other data fields that may be accessible to the UI

    TranningUiResult(List<VariableParcelable> data) {
        this.data = data;
    }


    List<VariableParcelable> getData() {
        return data;
    }
}
