package in.setone.appdm.ui.analize.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import in.setone.appdm.R;
import in.setone.appdm.ui.analize.ResultsModel;

import java.util.List;

public class ResultsAdapter extends RecyclerView.Adapter<ResultsAdapter.ResultViewHolder> {

    private List<ResultsModel> resultsModels;

    public ResultsAdapter(List<ResultsModel> resultsModels) {
        this.resultsModels = resultsModels;
    }

    @NonNull
    @Override
    public ResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recycler_view_item_list_results, parent, false);
        return new ResultViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ResultViewHolder holder, int position) {
        holder.title.setText(resultsModels.get(position).getNama_padi());
        holder.prop.setText(String.format("%.6f", resultsModels.get(position).getProbabilitas()));
        holder.num.setText(resultsModels.get(position).getNomor());
        holder.ung.setText(resultsModels.get(position).getKualitas());
    }

    @Override
    public int getItemCount() {
        return (resultsModels != null) ? resultsModels.size() : 0;
    }

    public class ResultViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private TextView title,prop,num,ung;
        public ResultViewHolder(@NonNull View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.imageItem);
            title = (TextView) itemView.findViewById(R.id.tvTitlePadi);
            prop = (TextView) itemView.findViewById(R.id.tvPropabilitasResult);
            num = (TextView) itemView.findViewById(R.id.tvNo);
            ung = (TextView) itemView.findViewById(R.id.tvUnggulan);
        }
    }
}
