package in.setone.appdm.ui.newtranning.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import in.setone.appdm.R;
import in.setone.appdm.data.newtranning.HimpunanModel;
import in.setone.appdm.data.newtranning.VariableModel;
import in.setone.appdm.database.model.HimpunanData;
import in.setone.appdm.database.model.KriteriaData;
import in.setone.appdm.database.model.VariableData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RecyclerViewAdapter extends RecyclerView.Adapter {

    List<VariableData> variable;
    List<HimpunanData> himpunan;
    List<KriteriaData> kriteria;
    Context mContext;
    List<VariableModel> vH = new ArrayList<>();
    protected ItemListener mListener;

    public RecyclerViewAdapter(Context context, List<VariableData> values, List<HimpunanData> values2, List<KriteriaData> values3, ItemListener itemListener) {
        kriteria = values3;
        variable = values;
        himpunan = values2;
        mContext = context;
        mListener = itemListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.recycler_view_item_gird, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder v = new ViewHolder(holder.itemView);
        Map<Integer, Map<Integer, String>> m = new HashMap<Integer, Map<Integer, String>>();
        Map<Integer, String> nn = new HashMap<Integer, String>();
        List<Integer> keyMaps = new ArrayList<>();
        List<Integer> keyMapsSub = new ArrayList<>();
        for (KriteriaData data : kriteria) {
            Map<Integer, String> c = new HashMap<Integer, String>();
            if (data.getKodevariabel() == variable.get(position).getId()) {
                if (!m.containsKey(data.getKodevariabel())) {
                    for (HimpunanData h : himpunan) {
                        if (data.getKodehimpunan() == h.getId()) {
                            c.put(h.getId(), h.getNama());
                            m.put(data.getKodevariabel(), c);
                            keyMaps.add(data.getKodevariabel());
                            keyMapsSub.add(h.getId());
                        }
                    }
                } else {
                    for (HimpunanData h : himpunan) {
                        if (data.getKodehimpunan() == h.getId()) {
                            Map<Integer, String> d = m.get(data.getKodevariabel());
                            m.remove(data.getKodevariabel());
                            d.put(h.getId(), h.getNama());
                            m.put(data.getKodevariabel(), d);
                            keyMapsSub.add(h.getId());
                        }
                    }
                }
            }
        }

        for (int xa = 0; xa < keyMaps.size(); xa++) {
            Map<Integer, String> d = m.get(keyMaps.get(xa));
            List<HimpunanModel> hs = new ArrayList<>();
            for (int ca = 0; ca < keyMapsSub.size(); ca++) {
                hs.add(new HimpunanModel(keyMapsSub.get(ca), d.get(keyMapsSub.get(ca)),0,0,0));
            }
            for (VariableData data : variable){
                if (data.getId() == keyMaps.get(xa)){
                    vH.add(new VariableModel(keyMaps.get(xa), data.getNama(), hs));
                }
            }
        }
        System.out.println(vH.toString());
        v.setData(vH.get(position));
    }

    @Override
    public int getItemCount() {
        return variable.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView textView;
        public TextView textView1;
        public ImageView imageView;
        public ConstraintLayout relativeLayout;
        VariableModel item;

        public ViewHolder(View v) {
            super(v);

            v.setOnClickListener(this);
            textView1 = (TextView) v.findViewById(R.id.titlevars);
            textView = (TextView) v.findViewById(R.id.tvHim);
            imageView = (ImageView) v.findViewById(R.id.imageView);
            relativeLayout = (ConstraintLayout) v.findViewById(R.id.relativeLayout);

        }

        public void setData(VariableModel item) {
            this.item = item;

            textView1.setText(item.getName());
            textView.setText(item.getHimpunan().get(0).getNama());
            imageView.setImageResource(R.drawable.img1);

        }


        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(item, textView, imageView);
            }
        }
    }

    public interface ItemListener {
        void onItemClick(VariableModel item, TextView textView, ImageView imageView);
    }
}
