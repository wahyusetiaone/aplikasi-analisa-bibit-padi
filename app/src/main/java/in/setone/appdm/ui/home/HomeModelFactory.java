package in.setone.appdm.ui.home;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import in.setone.appdm.data.home.HomeDatasource;
import in.setone.appdm.data.home.HomeRepository;
import in.setone.appdm.database.AppDatabase;

public class HomeModelFactory implements ViewModelProvider.Factory {
    private Context context;

    public HomeModelFactory(Context applicationContext) {
        this.context = applicationContext;
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(HomeViewModel.class ) && context != null) {
            return (T) new HomeViewModel(HomeRepository.getInstance(new HomeDatasource(), AppDatabase.getDatabase(context)));
        } else {
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}
