package in.setone.appdm.ui.listtranning.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import in.setone.appdm.R;
import in.setone.appdm.data.home.TranningDataModel;
import in.setone.appdm.data.home.VHdataModel;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListViewHolder> {

    List<TranningDataModel> list;

    public ListAdapter(List<TranningDataModel> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ListAdapter.ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recycler_view_item_list_tranning, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListAdapter.ListViewHolder holder, int position) {
        TranningDataModel item = list.get(position);
        List<VHdataModel> vhitem = item.getVarhim();
        holder.title.setText(item.getName());
        holder.code.setText(String.valueOf(item.getId()));

        for (int x=0; x < vhitem.size(); x++){
            VHdataModel model = vhitem.get(x);
            if (x == 0){
                holder.v1.setText(model.getVariable());
                holder.val1.setText(model.getHimpunan());
            }else if (x == 1){
                holder.v2.setText(model.getVariable());
                holder.val2.setText(model.getHimpunan());
            }else if (x == 2){
                holder.v3.setText(model.getVariable());
                holder.val3.setText(model.getHimpunan());
            }else if (x == 3){
                holder.v4.setText(model.getVariable());
                holder.val4.setText(model.getHimpunan());
            }
        }
    }

    @Override
    public int getItemCount() {
        return (list != null) ? list.size() : 0;
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        private TextView title, v1,v2,v3,v4,val1,val2,val3,val4,code;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.tvTitlePadiResult);
            v1 = (TextView) itemView.findViewById(R.id.tvHimpunan1);
            v2 = (TextView) itemView.findViewById(R.id.tvHimpunan2);
            v3 = (TextView) itemView.findViewById(R.id.tvHimpunan3);
            v4 = (TextView) itemView.findViewById(R.id.tvHimpunan4);
            val1 = (TextView) itemView.findViewById(R.id.tvVal1);
            val2 = (TextView) itemView.findViewById(R.id.tvVal2);
            val3 = (TextView) itemView.findViewById(R.id.tvVal3);
            val4 = (TextView) itemView.findViewById(R.id.tvVal4);
            code = (TextView) itemView.findViewById(R.id.tvCode);

        }
    }
}
