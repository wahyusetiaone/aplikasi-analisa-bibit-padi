package in.setone.appdm.ui.resultsanalize;

import android.content.Intent;
import android.util.Log;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import in.setone.appdm.R;
import in.setone.appdm.data.Result;
import in.setone.appdm.data.home.VHdataModel;
import in.setone.appdm.data.newtranning.HimpunanModel;
import in.setone.appdm.data.newtranning.ProbabilitasItem;
import in.setone.appdm.data.newtranning.VariableModel;
import in.setone.appdm.pacel.ProbabilitasParcel;
import in.setone.appdm.pacel.VariableProbabilitas;
import in.setone.appdm.pacel.VariableProbabilitasParcel;
import in.setone.appdm.ui.analize.FormAnalizeData;
import in.setone.appdm.ui.analize.ResultsModel;
import in.setone.appdm.ui.analize.adapter.ResultsAdapter;
import in.setone.appdm.ui.home.HomeActivity;
import in.setone.appdm.ui.home.adapter.ProbabilitasAdapter;

import java.time.Instant;
import java.util.*;

public class ResultsAnalizeActivity extends AppCompatActivity {

    private String TAG = "ResultsAnalizeActivity";
    private Map<Integer, Integer> _kriteria = new HashMap<Integer, Integer>();
    private List<ProbabilitasItem> probabilitasItems = new ArrayList<>();
    private List<VariableProbabilitas> variableProbabilitas = new ArrayList<>();
    private List<ResultsModel> modelList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ResultsAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results_analize);

        recyclerView = (RecyclerView) findViewById(R.id.rcResults);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);

        getIntentParcel();

        analize();

        adapter = new ResultsAdapter(modelList);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void analize() {
        Map<Integer, Integer> vc = new HashMap<Integer, Integer>();

        for (VariableProbabilitas data : variableProbabilitas) {
            vc.put(data.getVariable(), data.getHimpunan());
        }

        double _totalTranning = 0;
        for (ProbabilitasItem iteem : probabilitasItems) {
            List<VariableModel> modeel = iteem.getVariable();
            for (VariableModel v : modeel) {
                if (vc.containsKey(v.getId()) && v.getId() == 201) {
                    List<HimpunanModel> h = v.getHimpunan();
                    for (HimpunanModel hc : h) {
                        _totalTranning = _totalTranning + (double) hc.getJumlah();
                    }
                }
            }
        }

        for (ProbabilitasItem item : probabilitasItems) {
            double _padi = 0;
            List<VariableModel> model = item.getVariable();
            Map<Integer, Double> f = new HashMap<Integer, Double>();
            for (VariableModel v : model) {
                if (vc.containsKey(v.getId())) {
                    int cs = vc.get(v.getId());
                    List<HimpunanModel> h = v.getHimpunan();
                    double g = 0;
                    double s = 0;
                    for (HimpunanModel hc : h) {
                        if (hc.getId() == cs) {
                            g = (double) hc.getJumlah();
                        }
                        s = s + (double) hc.getJumlah();
                    }
                    double a = g / s;
//                    Log.d(TAG, "Nilai g : "+String.format("%.4f", g)+",  Nilai s : "+String.format("%.4f", s)+", Result : "+String.format("%.4f", a));
                    f.put(v.getId(), a);
                    _padi = s;
                }
            }
            double probabilitasPadi = (double) 0;

            for (Map.Entry<Integer, Double> sa : f.entrySet()) {
                Log.d(TAG, "Nilai  : " + String.format("%.4f", sa.getValue()));
                if (probabilitasPadi == (double) 0) {
                    probabilitasPadi = sa.getValue();
                } else {
                    probabilitasPadi = probabilitasPadi * sa.getValue();
                }
            }
            double pr = (double) (_padi / _totalTranning);
            probabilitasPadi = probabilitasPadi * pr;
            Log.d(TAG, "Prop Padi " + item.getId() + " : " + probabilitasPadi);
            modelList.add(new ResultsModel(item.getId(), item.getId(), item.getNama(), probabilitasPadi, "Not Yet"));
        }

        List<Double> lss = new ArrayList<>();
        for (ResultsModel daf : modelList) {
            if (!lss.contains(daf.getProbabilitas())) {
                lss.add(daf.getProbabilitas());
            }
        }

        Collections.sort(lss,Collections.reverseOrder());

        Log.d(TAG, lss.toString());

        List<ResultsModel> newList = new ArrayList<>();
        double _top = lss.get(0);
        int po = 1;
        for (ResultsModel rm : modelList) {
            String kk = "Bibit Kurang Baik";
            for (Double las : lss) {
                if (rm.getProbabilitas() == _top) {
                    kk = "Bibit Baik";
                }
                if (rm.getProbabilitas() == las) {
                    newList.add(new ResultsModel(po, rm.getKode_padi(), rm.getNama_padi(), rm.getProbabilitas(), kk));
                    po++;
                }
            }
        }
        Log.d(TAG, newList.toString());
        modelList.clear();
        modelList.addAll(newList);
    }

    private void getIntentParcel() {

        Intent intent = getIntent();
        ProbabilitasParcel dt = intent.getParcelableExtra(FormAnalizeData.KEY_SENDER1);
        dt.setClassLoader(FormAnalizeData.class.getClassLoader());
        probabilitasItems.addAll(dt.getList());
        Log.d(TAG, probabilitasItems.toString());

        VariableProbabilitasParcel dta = intent.getParcelableExtra(FormAnalizeData.KEY_SENDER2);
        dta.setClassLoader(FormAnalizeData.class.getClassLoader());
        variableProbabilitas.addAll(dta.getList());
        Log.d(TAG, variableProbabilitas.toString());
    }
}
