package in.setone.appdm.ui.newtranning;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import in.setone.appdm.data.home.HomeDatasource;
import in.setone.appdm.data.home.HomeRepository;
import in.setone.appdm.data.newtranning.TranningDatasource;
import in.setone.appdm.data.newtranning.TranningRepository;
import in.setone.appdm.database.AppDatabase;

public class TranningModelFactory implements ViewModelProvider.Factory {
    private Context context;

    public TranningModelFactory(Context applicationContext) {
        this.context = applicationContext;
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(TranningViewModel.class ) && context != null) {
            return (T) new TranningViewModel(TranningRepository.getInstance(new TranningDatasource(), AppDatabase.getDatabase(context)));
        } else {
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}
