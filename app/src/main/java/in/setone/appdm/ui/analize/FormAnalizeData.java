package in.setone.appdm.ui.analize;

import android.content.Intent;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import in.setone.appdm.R;
import in.setone.appdm.data.home.TranningDataModel;
import in.setone.appdm.data.newtranning.HimpunanModel;
import in.setone.appdm.data.newtranning.ProbabilitasItem;
import in.setone.appdm.data.newtranning.TranningModel;
import in.setone.appdm.data.newtranning.VariableModel;
import in.setone.appdm.database.AppDatabase;
import in.setone.appdm.database.model.*;
import in.setone.appdm.pacel.ProbabilitasParcel;
import in.setone.appdm.pacel.TranningDetailsParcel;
import in.setone.appdm.pacel.VariableProbabilitas;
import in.setone.appdm.pacel.VariableProbabilitasParcel;
import in.setone.appdm.ui.home.HomeActivity;
import in.setone.appdm.ui.listtranning.ListTranningActivity;
import in.setone.appdm.ui.newtranning.FormDataTranningActivity;
import in.setone.appdm.ui.newtranning.TranningModelFactory;
import in.setone.appdm.ui.newtranning.TranningViewModel;
import in.setone.appdm.ui.newtranning.adapter.RecyclerViewAdapter;
import in.setone.appdm.ui.resultsanalize.ResultsAnalizeActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FormAnalizeData extends AppCompatActivity implements RecyclerViewAdapter.ItemListener {

    public static final String KEY_SENDER1 = "THISVERYSCURE1";
    public static final String KEY_SENDER2 = "THISVERYSCURE2";
    private AppDatabase appDatabase;
    private TranningViewModel viewModel;
    private String TAG = "FormAnalizeDataActivity";
    private List<PadiData> padi = new ArrayList<>();
    private List<TranningData> tranning = new ArrayList<>();
    private List<VariableData> variable = new ArrayList<>();
    private List<HimpunanData> himpunan = new ArrayList<>();
    private List<KriteriaData> kriteria = new ArrayList<>();
    private Map<Integer, Integer> _index = new HashMap<Integer, Integer>();
    private Map<Integer, Integer> _kriteria = new HashMap<Integer, Integer>();
    private Integer _lasted = 0;
    private List<ProbabilitasItem> probabilitasItems = new ArrayList<>();
    private List<VariableProbabilitas> variableProbabilitas = new ArrayList<>();
    RecyclerView recyclerView;
    CardView cardAnalize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_analize_data);

        cardAnalize = (CardView) findViewById(R.id.cardAnalizeData);

        getIntentParcel();

        setup();

        viewModel = ViewModelProviders.of(this, new TranningModelFactory(getApplicationContext()))
                .get(TranningViewModel.class);


        appDatabase = AppDatabase.getDatabase(this);
        appDatabase.getOpenHelper().getWritableDatabase();

        reqData(this);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewAnalize);
        final RecyclerViewAdapter adapter = new RecyclerViewAdapter(this, variable, himpunan, kriteria, this);
        recyclerView.setAdapter(adapter);
        GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        ;

        new CountDownTimer(5000, 1000) {

            public void onTick(long millisUntilFinished) {
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                if (!viewModel.checkingData()) {
                    reqData(FormAnalizeData.this);
                } else {
                    viewModel.dataResult().observe(FormAnalizeData.this, new Observer<TranningModel>() {
                        @Override
                        public void onChanged(TranningModel tranningModel) {
                            variable.addAll(tranningModel.getDataVariable());
                            himpunan.addAll(tranningModel.getDataHimpunan());
                            kriteria.addAll(tranningModel.getDataKriteria());
                            adapter.notifyDataSetChanged();

                            for (KriteriaData data : kriteria) {
                                if (!_kriteria.containsKey(data.getKodevariabel())) {
                                    _kriteria.put(data.getKodevariabel(), data.getKodehimpunan());
                                }
                            }
                        }
                    });
                }
                cardAnalize.setVisibility(View.VISIBLE);
            }
        }.start();

        cardAnalize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (Map.Entry<Integer, Integer> ds : _kriteria.entrySet()){
                    variableProbabilitas.add(new VariableProbabilitas(ds.getKey(),ds.getValue()));
                }

                if (variableProbabilitas.size() != 0){
                    Intent i = new Intent(FormAnalizeData.this, ResultsAnalizeActivity.class);
                    i.putExtra(KEY_SENDER1, new ProbabilitasParcel(probabilitasItems));
                    i.putExtra(KEY_SENDER2, new VariableProbabilitasParcel(variableProbabilitas));
                    startActivity(i);
                }
            }
        });
    }

    private void getIntentParcel() {

        Intent intent = getIntent();
        ProbabilitasParcel dt = intent.getParcelableExtra(HomeActivity.KEY_SENDER);
        dt.setClassLoader(FormAnalizeData.class.getClassLoader());
        probabilitasItems.addAll(dt.getList());
        Log.d(TAG,probabilitasItems.toString());
    }

    private void setup() {
        _kriteria.put(201,301);
        _kriteria.put(202,303);
        _kriteria.put(203,303);
        _kriteria.put(204,303);
    }

    private void reqData(FormAnalizeData activity) {
        viewModel.padiData(activity);
        viewModel.variableData(activity);
        viewModel.himpunanData(activity);
        viewModel.kriteriaData(activity);
        viewModel.getLastedID().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                _lasted = integer;
            }
        });
    }

    @Override
    public void onItemClick(VariableModel item, TextView textView, ImageView imageView) {
        List<HimpunanModel> himpunan = item.getHimpunan();
        System.out.println(himpunan.toString());
        int s = himpunan.size();
        int c = 0;
        if (_index.containsKey(item.getId())) {
            System.out.println("Repeat ");
            c = _index.get(item.getId());
            for (HimpunanModel hca : himpunan) {
                if (textView.getText().equals(hca.getNama())) {
                    c++;
                    if (c < s) {
                        textView.setText(himpunan.get(c).getNama());
                        System.out.println("code " + c + " this text : " + himpunan.get(c).getNama());
                        _index.remove(item.getId());
                        _index.put(item.getId(), c);
                        break;
                    } else {
                        textView.setText(himpunan.get(0).getNama());
                        System.out.println("code not found " + c + " this text back : " + himpunan.get(0).getNama());
                        c = 0;
                        _index.remove(item.getId());
                        _index.put(item.getId(), c);
                        break;
                    }

                }
            }
        } else {
            System.out.println("Begin ");
            _index.put(item.getId(), c);
            for (HimpunanModel hca : himpunan) {
                if (textView.getText().equals(hca.getNama())) {
                    c++;
                    if (c < s) {
                        textView.setText(himpunan.get(c).getNama());
                        System.out.println("code " + _index + " this text : " + himpunan.get(c).getNama());
                        _index.remove(item.getId());
                        _index.put(item.getId(), c);
                        break;
                    } else {
                        textView.setText(himpunan.get(0).getNama());
                        System.out.println("code not found " + c + " this text back : " + himpunan.get(0).getNama());
                        c = 0;
                        _index.remove(item.getId());
                        _index.put(item.getId(), c);
                        break;
                    }

                }
            }
        }
        Toast.makeText(getApplicationContext(), item.getName() + " is clicked ", Toast.LENGTH_SHORT).show();
        //TODO:Harus di klik, kalo tidak gak akan terdaftar. simple problem but danger !!!!
        if (_kriteria.containsKey(item.getId())) {
            _kriteria.remove(item.getId());
            _kriteria.put(item.getId(), himpunan.get(c).getId());
        }
        System.out.println("Kriteria " + _kriteria.toString() + " Index "+_index.toString());
    }
}
