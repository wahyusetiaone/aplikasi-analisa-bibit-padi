package in.setone.appdm.ui.newtranning.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import in.setone.appdm.database.model.HimpunanData;
import in.setone.appdm.database.model.VariableData;

import java.util.List;

public class VariableAdapter extends BaseAdapter {
    private final Context mContext;
    private final List<VariableData> variableData;
    private final List<HimpunanData> himpunanData;

    // 1
    public VariableAdapter(Context context, List<VariableData> variableData, List<HimpunanData> himpunanData) {
        this.mContext = context;
        this.variableData = variableData;
        this.himpunanData = himpunanData;
    }

    // 2
    @Override
    public int getCount() {
        return variableData.size();
    }

    // 3
    @Override
    public long getItemId(int position) {
        return 0;
    }

    // 4
    @Override
    public Object getItem(int position) {
        return null;
    }

    // 5
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView dummyTextView = new TextView(mContext);
        dummyTextView.setText(String.valueOf(position));
        return dummyTextView;
    }
}
