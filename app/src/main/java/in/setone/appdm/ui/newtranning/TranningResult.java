package in.setone.appdm.ui.newtranning;

import androidx.annotation.Nullable;

/**
 * Authentication result : success (user details) or error message.
 */
class TranningResult {
    @Nullable
    private TranningUiResult success;
    @Nullable
    private Integer error;

    TranningResult(@Nullable Integer error) {
        this.error = error;
    }

    TranningResult(@Nullable TranningUiResult success) {
        this.success = success;
    }

    @Nullable
    TranningUiResult getSuccess() {
        return success;
    }

    @Nullable
    Integer getError() {
        return error;
    }
}
