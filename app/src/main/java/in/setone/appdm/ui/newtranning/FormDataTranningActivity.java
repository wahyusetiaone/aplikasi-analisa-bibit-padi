package in.setone.appdm.ui.newtranning;

import android.content.Intent;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import in.setone.appdm.R;
import in.setone.appdm.data.newtranning.HimpunanModel;
import in.setone.appdm.data.newtranning.TranningModel;
import in.setone.appdm.data.newtranning.VariableModel;
import in.setone.appdm.database.AppDatabase;
import in.setone.appdm.database.model.*;
import in.setone.appdm.ui.home.HomeActivity;
import in.setone.appdm.ui.newtranning.adapter.RecyclerViewAdapter;

import java.util.*;

public class FormDataTranningActivity extends AppCompatActivity implements RecyclerViewAdapter.ItemListener {


    private AppDatabase appDatabase;
    private TranningViewModel viewModel;
    private String TAG = "FormDataTranningActivity";
    private List<PadiData> padi = new ArrayList<>();
    private List<VariableData> variable = new ArrayList<>();
    private List<HimpunanData> himpunan = new ArrayList<>();
    private List<KriteriaData> kriteria = new ArrayList<>();
    private Map<Integer, Integer> _index = new HashMap<Integer, Integer>();
    private Map<Integer, Integer> _kriteria = new HashMap<Integer, Integer>();
    private Integer _padi = 101;
    private Integer _lasted = 0;

    RecyclerView recyclerView;
    CardView cardPlus, cardPadi;
    TextView namapadi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_data_tranning);

        cardPadi = (CardView) findViewById(R.id.cardPadi);
        cardPlus = (CardView) findViewById(R.id.cardAnalize);
        namapadi = (TextView) findViewById(R.id.namaPadi);

        setup();

        viewModel = ViewModelProviders.of(this, new TranningModelFactory(getApplicationContext()))
                .get(TranningViewModel.class);


        appDatabase = AppDatabase.getDatabase(this);
        appDatabase.getOpenHelper().getWritableDatabase();

        reqData(this);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewAnalize);
        final RecyclerViewAdapter adapter = new RecyclerViewAdapter(this, variable, himpunan, kriteria, this);
        recyclerView.setAdapter(adapter);
        GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        ;

        new CountDownTimer(5000, 1000) {

            public void onTick(long millisUntilFinished) {
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                if (!viewModel.checkingData()) {
                    reqData(FormDataTranningActivity.this);
                } else {
                    viewModel.dataResult().observe(FormDataTranningActivity.this, new Observer<TranningModel>() {
                        @Override
                        public void onChanged(TranningModel tranningModel) {
                            padi.addAll(tranningModel.getDataPadi());
                            variable.addAll(tranningModel.getDataVariable());
                            himpunan.addAll(tranningModel.getDataHimpunan());
                            kriteria.addAll(tranningModel.getDataKriteria());
                            adapter.notifyDataSetChanged();

                            for (KriteriaData data : kriteria) {
                                if (!_kriteria.containsKey(data.getKodevariabel())) {
                                    _kriteria.put(data.getKodevariabel(), data.getKodehimpunan());
                                }
                            }
                        }
                    });
                }
                cardPadi.setVisibility(View.VISIBLE);
                cardPlus.setVisibility(View.VISIBLE);
            }

        }.start();

        cardPadi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAction();
            }
        });

        cardPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i = 10001 + _lasted;
                String query = "";
                int x = 0;
                int c = _kriteria.size();
                for ( Integer key : _kriteria.keySet() ) {
                    String o = "5" + String.valueOf(key) + String.valueOf(_kriteria.get(key));
                    if (x != (c-1)) {
                        query = query + o + ",";
                    } else {
                        query = query + o;
                    }
                    x++;
                }
                viewModel.saveTranning(new TranningData(i, _padi, query));
                Intent intent = new Intent(FormDataTranningActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private void setup() {
        _kriteria.put(201,301);
        _kriteria.put(202,303);
        _kriteria.put(203,303);
        _kriteria.put(204,303);
    }

    private void onAction() {
        int s = padi.size();
        int c = _padi;
        String nma = "";
        for (PadiData data : padi) {
            if (_padi == data.getId()) {
                System.out.println("Repeat ");
                if (c != padi.get(s - 1).getId()) {
                    c++;
                } else {
                    c = padi.get(0).getId();
                    namapadi.setText(padi.get(0).getNama());
                    nma = padi.get(0).getNama();
                    break;
                }
            } else {
                if (data.getId() == c) {
                    namapadi.setText(data.getNama());
                    nma = data.getNama();
                    break;
                }
            }
        }
        Toast.makeText(getApplicationContext(), nma + " is clicked ", Toast.LENGTH_SHORT).show();
        //TODO:Harus di klik, kalo tidak gak akan terdaftar. simple problem but danger !!!!
        _padi = c;
        System.out.println("Padi " + _padi.toString());
    }

    private void reqData(FormDataTranningActivity activity) {
        viewModel.padiData(activity);
        viewModel.variableData(activity);
        viewModel.himpunanData(activity);
        viewModel.kriteriaData(activity);
        viewModel.getLastedID().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                _lasted = integer;
            }
        });
    }

    @Override
    public void onItemClick(VariableModel item, TextView textView, ImageView imageView) {
        List<HimpunanModel> himpunan = item.getHimpunan();
        System.out.println(himpunan.toString());
        int s = himpunan.size();
        int c = 0;
        if (_index.containsKey(item.getId())) {
            System.out.println("Repeat ");
            c = _index.get(item.getId());
            for (HimpunanModel hca : himpunan) {
                if (textView.getText().equals(hca.getNama())) {
                    c++;
                    if (c < s) {
                        textView.setText(himpunan.get(c).getNama());
                        System.out.println("code " + c + " this text : " + himpunan.get(c).getNama());
                        _index.remove(item.getId());
                        _index.put(item.getId(), c);
                        break;
                    } else {
                        textView.setText(himpunan.get(0).getNama());
                        System.out.println("code not found " + c + " this text back : " + himpunan.get(0).getNama());
                        c = 0;
                        _index.remove(item.getId());
                        _index.put(item.getId(), c);
                        break;
                    }

                }
            }
        } else {
            System.out.println("Begin ");
            _index.put(item.getId(), c);
            for (HimpunanModel hca : himpunan) {
                if (textView.getText().equals(hca.getNama())) {
                    c++;
                    if (c < s) {
                        textView.setText(himpunan.get(c).getNama());
                        System.out.println("code " + _index + " this text : " + himpunan.get(c).getNama());
                        _index.remove(item.getId());
                        _index.put(item.getId(), c);
                        break;
                    } else {
                        textView.setText(himpunan.get(0).getNama());
                        System.out.println("code not found " + c + " this text back : " + himpunan.get(0).getNama());
                        c = 0;
                        _index.remove(item.getId());
                        _index.put(item.getId(), c);
                        break;
                    }

                }
            }
        }
        Toast.makeText(getApplicationContext(), item.getName() + " is clicked ", Toast.LENGTH_SHORT).show();
        //TODO:Harus di klik, kalo tidak gak akan terdaftar. simple problem but danger !!!!
        if (_kriteria.containsKey(item.getId())) {
            _kriteria.remove(item.getId());
            _kriteria.put(item.getId(), himpunan.get(c).getId());
        }
        System.out.println("Kriteria " + _kriteria.toString() + " Index "+_index.toString());
    }
}
