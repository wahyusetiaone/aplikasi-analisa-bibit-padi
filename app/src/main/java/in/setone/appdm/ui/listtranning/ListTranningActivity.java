package in.setone.appdm.ui.listtranning;

import android.app.ListActivity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import in.setone.appdm.R;
import in.setone.appdm.data.home.TranningDataModel;
import in.setone.appdm.data.newtranning.ProbabilitasItem;
import in.setone.appdm.pacel.TranningDetailsParcel;
import in.setone.appdm.ui.home.HomeActivity;
import in.setone.appdm.ui.home.adapter.ProbabilitasAdapter;
import in.setone.appdm.ui.listtranning.adapter.ListAdapter;
import in.setone.appdm.ui.newtranning.FormDataTranningActivity;

import java.util.ArrayList;
import java.util.List;

public class ListTranningActivity extends AppCompatActivity {

    private final String TAG = "ListTranningActivity";
    private FloatingActionButton tmbahdata;
    private RecyclerView recyclerView;
    private ListAdapter adapter;
    private List<TranningDataModel> tranningItemDetails = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_tranning);

        tmbahdata = (FloatingActionButton) findViewById(R.id.floTambahTranning);

        Intent intent = getIntent();
        TranningDetailsParcel dt = intent.getParcelableExtra(HomeActivity.KEY_SENDER);
        dt.setClassLoader(FormDataTranningActivity.class.getClassLoader());
        tranningItemDetails.addAll(dt.getList());

        recyclerView = (RecyclerView) findViewById(R.id.rcList);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);

        adapter = new in.setone.appdm.ui.listtranning.adapter.ListAdapter(tranningItemDetails);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        tmbahdata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ListTranningActivity.this,FormDataTranningActivity.class);
                startActivity(i);
                finish();
            }
        });
    }
}
