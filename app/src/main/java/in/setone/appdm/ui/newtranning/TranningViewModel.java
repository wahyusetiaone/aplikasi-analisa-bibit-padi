package in.setone.appdm.ui.newtranning;

import android.util.Log;
import androidx.lifecycle.*;
import in.setone.appdm.data.Result;
import in.setone.appdm.data.home.HomeDatasource;
import in.setone.appdm.data.home.HomeModel;
import in.setone.appdm.data.newtranning.TranningDatasource;
import in.setone.appdm.data.newtranning.TranningModel;
import in.setone.appdm.data.newtranning.TranningRepository;
import in.setone.appdm.database.model.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TranningViewModel extends ViewModel {

    private TranningRepository tranningRepository;

    private static String TAG = "TranningViewModel";

    private MutableLiveData<TranningModel> mutableLiveData = new MutableLiveData<>();

    private List<PadiData> padi = new ArrayList<>();
    private List<VariableData> variable = new ArrayList<>();
    private List<HimpunanData> himpunan = new ArrayList<>();
    private List<KriteriaData> kriteria = new ArrayList<>();

    private String _err = "";

    private String[] s = new String[1];

    TranningViewModel(TranningRepository tranningRepository) {
        this.tranningRepository = tranningRepository;
    }

    public interface CallbackProbabilitasVariable {
        void onResult(String[] strings);
    }

    public void padiData(final LifecycleOwner owner) {
        tranningRepository.dataPadi(owner, new TranningDatasource.CallBackPadiDatasource() {
            @Override
            public void onCallpadi(Result<List<PadiData>> result) {
                if (result instanceof Result.Success) {
                    padi.clear();
                    padi.addAll((List<PadiData>) ((Result.Success) result).getData());

                    Log.d(TAG, "Result of Data Padi : " + padi.toString());
                }

                if (result instanceof Result.Error) {
                    _err = _err + " | Variable";
                }

                Log.d(TAG, "Loading Data Padi");
                mutableLiveData.setValue(new TranningModel(padi,variable, himpunan, kriteria));
            }
        });
    }

    public void variableData(final LifecycleOwner owner) {
        tranningRepository.dataVariable(owner, new TranningDatasource.CallBackVariableDatasource() {
            @Override
            public void onCallVariable(Result<List<VariableData>> result) {
                if (result instanceof Result.Success) {
                    variable.clear();
                    variable.addAll((List<VariableData>) ((Result.Success) result).getData());

                    Log.d(TAG, "Result of Data Variable : " + variable.toString());
                }

                if (result instanceof Result.Error) {
                    _err = _err + " | Variable";
                }

                Log.d(TAG, "Loading Data Variable");
                mutableLiveData.setValue(new TranningModel(padi,variable, himpunan, kriteria));
            }
        });
    }

    public void himpunanData(final LifecycleOwner owner) {
        tranningRepository.dataHimpunan(owner, new TranningDatasource.CallBackHimpunanDatasource() {
            @Override
            public void onCallHimpunan(Result<List<HimpunanData>> result) {
                if (result instanceof Result.Success) {
                    himpunan.clear();
                    himpunan.addAll((List<HimpunanData>) ((Result.Success) result).getData());

                    Log.d(TAG, "Result of Data Himpunan : " + himpunan.toString());
                }

                if (result instanceof Result.Error) {
                    _err = _err + " | Himpunan";
                }

                Log.d(TAG, "Loading Data Himpunan");
                mutableLiveData.setValue(new TranningModel(padi,variable, himpunan, kriteria));
            }
        });
    }

    public void kriteriaData(final LifecycleOwner owner) {
        tranningRepository.dataKriteria(owner, new TranningDatasource.CallBackKriteriaDatasource() {
            @Override
            public void onCallKriteria(Result<List<KriteriaData>> result) {
                if (result instanceof Result.Success) {
                    kriteria.clear();
                    kriteria.addAll((List<KriteriaData>) ((Result.Success) result).getData());

                    Log.d(TAG, "Result of Data Himpunan : " + himpunan.toString());
                }

                if (result instanceof Result.Error) {
                    _err = _err + " | Himpunan";
                }

                Log.d(TAG, "Loading Data Himpunan");
                mutableLiveData.setValue(new TranningModel(padi,variable, himpunan, kriteria));
            }
        });
    }

    public boolean checkingData() {
        if (variable.size() != 0 || himpunan.size() != 0) {
            return true;
        }
        return false;
    }

    public MutableLiveData<TranningModel> dataResult() {
        return mutableLiveData;
    }

    public LiveData<Integer> getLastedID(){
        return tranningRepository.getLastedID();
    }

    public void saveTranning(TranningData tranningData){
        tranningRepository.insertDataTranning(tranningData);
    }
}
