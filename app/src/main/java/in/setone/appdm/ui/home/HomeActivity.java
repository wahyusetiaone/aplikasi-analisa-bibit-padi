package in.setone.appdm.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.LinearLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import in.setone.appdm.R;
import in.setone.appdm.data.home.HomeModel;
import in.setone.appdm.data.home.TranningDataModel;
import in.setone.appdm.data.newtranning.ProbabilitasItem;
import in.setone.appdm.database.AppDatabase;
import in.setone.appdm.database.model.HimpunanData;
import in.setone.appdm.database.room.HimpunanDao;
import in.setone.appdm.pacel.ProbabilitasParcel;
import in.setone.appdm.pacel.TranningDetailsParcel;
import in.setone.appdm.ui.analize.FormAnalizeData;
import in.setone.appdm.ui.home.adapter.ProbabilitasAdapter;
import in.setone.appdm.ui.listtranning.ListTranningActivity;

import java.util.*;

public class HomeActivity extends AppCompatActivity {
    public static final String KEY_SENDER = "THISVERYSCURE";

    private AppDatabase appDatabase;
    private HomeViewModel viewModelHome;
    private HimpunanDao himpunanDao;
    private String TAG = "HomeActivity";
    private List<HimpunanData> himpunan = new ArrayList<>();

    private LinearLayout btnAddTranning;
    private LinearLayout btnAnalize;

    private RecyclerView recyclerView;
    private ProbabilitasAdapter adapter;
    private List<ProbabilitasItem> probabilitasItems = new ArrayList<>();
    private List<TranningDataModel> tranningItemDetails = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        viewModelHome = ViewModelProviders.of(this, new HomeModelFactory(getApplicationContext()))
                .get(HomeViewModel.class);

        btnAddTranning = (LinearLayout) findViewById(R.id.menu1);
        btnAnalize = (LinearLayout) findViewById(R.id.menu3);

        appDatabase = AppDatabase.getDatabase(this);
        appDatabase.getOpenHelper().getWritableDatabase();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewAnalize);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);

        reqData(this);


        new CountDownTimer(5000, 1000) {

            public void onTick(long millisUntilFinished) {
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                if (!viewModelHome.checkingData()){
                    reqData(HomeActivity.this);
                }else {
                    viewModelHome.dataResult().observe(HomeActivity.this, new Observer<HomeModel>() {
                        @Override
                        public void onChanged(HomeModel homeModel) {
                            viewModelHome.probabilitasVariable(homeModel, new HomeViewModel.CallbackProbabilitas() {
                                @Override
                                public void onResult(List<ProbabilitasItem> probabilitasItemss) {
                                    probabilitasItems.clear();
                                    probabilitasItems.addAll(probabilitasItemss);
                                    adapter = new ProbabilitasAdapter(probabilitasItems.get(0).getVariable());
                                    recyclerView.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                    Log.d(TAG, probabilitasItems.toString());
                                }
                            });
                            viewModelHome.listDetailsTranning(homeModel, new HomeViewModel.CallbackTranningDetalis() {
                                @Override
                                public void onResult(List<TranningDataModel> t) {
                                    tranningItemDetails.clear();
                                    tranningItemDetails.addAll(t);
                                }
                            });
                        }
                    });

                }
            }

        }.start();

        btnAddTranning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, tranningItemDetails.toString());

                Intent i = new Intent(HomeActivity.this, ListTranningActivity.class);
                i.putExtra(KEY_SENDER, new TranningDetailsParcel(tranningItemDetails));
                startActivity(i);
            }
        });

        btnAnalize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HomeActivity.this, FormAnalizeData.class);
                i.putExtra(KEY_SENDER, new ProbabilitasParcel(probabilitasItems));
                startActivity(i);
            }
        });
    }

    private void reqData(HomeActivity homeActivity) {
        viewModelHome.padiData(homeActivity);
        viewModelHome.tranningData(homeActivity);
        viewModelHome.kriteriaData(homeActivity);
        viewModelHome.variableData(homeActivity);
        viewModelHome.himpunanData(homeActivity);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
