package in.setone.appdm.ui.splash;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProviders;
import in.setone.appdm.R;
import in.setone.appdm.ui.home.HomeActivity;
import in.setone.appdm.ui.home.HomeModelFactory;
import in.setone.appdm.ui.home.HomeViewModel;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashscreenActivity extends AppCompatActivity {

    private HomeViewModel viewModelHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splashscreen);
        viewModelHome = ViewModelProviders.of(this, new HomeModelFactory(getApplicationContext()))
                .get(HomeViewModel.class);


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashscreenActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        }, 3000);

    }
}
