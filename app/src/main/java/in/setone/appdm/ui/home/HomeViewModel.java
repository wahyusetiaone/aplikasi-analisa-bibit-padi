package in.setone.appdm.ui.home;

import android.app.Activity;
import android.content.Intent;
import android.os.CountDownTimer;
import android.util.Log;
import androidx.lifecycle.*;
import in.setone.appdm.data.Result;
import in.setone.appdm.data.home.*;
import in.setone.appdm.data.newtranning.HimpunanModel;
import in.setone.appdm.data.newtranning.ProbabilitasItem;
import in.setone.appdm.data.newtranning.VariableModel;
import in.setone.appdm.database.model.*;
import in.setone.appdm.database.room.HimpunanDao;
import in.setone.appdm.database.room.PadiDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeViewModel extends ViewModel {

    private HomeRepository homeRepository;

    private MutableLiveData<HomeModel> homeModelMutableLiveData = new MutableLiveData<>();

    private MutableLiveData<HomeResult> resultMutableLiveData = new MutableLiveData<>();

    private static String TAG = "HomeViewModel";
    private List<ProbabilitasItem> probabilitasItems = new ArrayList<>();
    private List<TranningDataModel> tranningItemDetails = new ArrayList<>();

    private int[] POINT_OF_CLASS = new int[3];

    private List<PadiData> padi = new ArrayList<>();
    private List<TranningData> tranning = new ArrayList<>();
    private List<KriteriaData> kriteria = new ArrayList<>();
    private List<VariableData> variable = new ArrayList<>();
    private List<HimpunanData> himpunan = new ArrayList<>();
    //nama padi
    private Map<Integer, String> _nameofpadi = new HashMap<Integer, String>();

    private String _err = "";

    private String[] s = new String[4];

    HomeViewModel(HomeRepository homeRepository) {
        this.homeRepository = homeRepository;
    }

    public interface CallbackTranningDetalis {
        void onResult(List<TranningDataModel> tranningDataModelList);
    }

    public interface CallbackProbabilitas {
        void onResult(List<ProbabilitasItem> probabilitasItems);
    }

    public void padiData(final LifecycleOwner owner) {
        homeRepository.datapadi(owner, new HomeDatasource.CallBackPadiDatasource() {
            @Override
            public void onCallPadi(Result<List<PadiData>> result) {
                if (result instanceof Result.Success) {
                    padi.clear();
                    padi.addAll((List<PadiData>) ((Result.Success) result).getData());

                    Log.d(TAG, "Result of Data Padi : " + padi.toString());
                }

                if (result instanceof Result.Error) {
                    _err = _err + " | Padi";
                }
                Log.d(TAG, "Loading Data Padi");
                homeModelMutableLiveData.setValue(new HomeModel(padi,tranning, kriteria, variable, himpunan));
            }
        });
    }

    public void tranningData(final LifecycleOwner owner) {
        homeRepository.dataTranning(owner, new HomeDatasource.CallBackTranningDatasource() {
            @Override
            public void onCallTranning(Result<List<TranningData>> result) {
                if (result instanceof Result.Success) {
                    tranning.clear();
                    tranning.addAll((List<TranningData>) ((Result.Success) result).getData());

                    Log.d(TAG, "Result of Data Tranning : " + tranning.toString());
                }

                if (result instanceof Result.Error) {
                    _err = _err + " | Tranning";
                }
                Log.d(TAG, "Loading Data Tranning");
                homeModelMutableLiveData.setValue(new HomeModel(padi,tranning, kriteria, variable, himpunan));
            }
        });
    }

    public void kriteriaData(final LifecycleOwner owner) {
        homeRepository.dataKriteria(owner, new HomeDatasource.CallBackKriteriaDatasource() {
            @Override
            public void onCallKriteria(Result<List<KriteriaData>> result) {
                if (result instanceof Result.Success) {
                    kriteria.clear();
                    kriteria.addAll((List<KriteriaData>) ((Result.Success) result).getData());

                    Log.d(TAG, "Result of Data Kriteria : " + kriteria.toString());
                }

                if (result instanceof Result.Error) {
                    _err = _err + " | Kriteria";
                }
                Log.d(TAG, "Loading Data Kriteria");
                homeModelMutableLiveData.setValue(new HomeModel(padi,tranning, kriteria, variable, himpunan));
            }
        });
    }

    public void variableData(final LifecycleOwner owner) {
        homeRepository.datavariable(owner, new HomeDatasource.CallBackVariableDatasource() {
            @Override
            public void onCallVariable(Result<List<VariableData>> result) {
                if (result instanceof Result.Success) {
                    variable.clear();
                    variable.addAll((List<VariableData>) ((Result.Success) result).getData());

                    Log.d(TAG, "Result of Data Variable : " + variable.toString());
                }

                if (result instanceof Result.Error) {
                    _err = _err + " | Variable";
                }

                Log.d(TAG, "Loading Data Variable");
                homeModelMutableLiveData.setValue(new HomeModel(padi,tranning, kriteria, variable, himpunan));
            }
        });
    }

    public void himpunanData(final LifecycleOwner owner) {
        homeRepository.dataHimpunan(owner, new HomeDatasource.CallBackHimpunanDatasource() {
            @Override
            public void onCallHimpunan(Result<List<HimpunanData>> result) {
                if (result instanceof Result.Success) {
                    himpunan.clear();
                    himpunan.addAll((List<HimpunanData>) ((Result.Success) result).getData());

                    Log.d(TAG, "Result of Data Himpunan : " + himpunan.toString());
                }

                if (result instanceof Result.Error) {
                    _err = _err + " | Himpunan";
                }

                Log.d(TAG, "Loading Data Himpunan");
                homeModelMutableLiveData.setValue(new HomeModel(padi,tranning, kriteria, variable, himpunan));
            }
        });
    }

    public boolean checkingData() {
        if (tranning.size() != 0 || kriteria.size() != 0 || variable.size() != 0 || himpunan.size() != 0) {
            return true;
        }
        return false;
    }

    public MutableLiveData<HomeModel> dataResult() {
        return homeModelMutableLiveData;
    }

    public void probabilitasVariable(HomeModel homeModel, final HomeViewModel.CallbackProbabilitas callbackProbabilitas) {

        POINT_OF_CLASS[0] = 204;
        POINT_OF_CLASS[1] = 303;
        POINT_OF_CLASS[2] = 304;

        for (PadiData dat2 : homeModel.getDataPadi()){
            if (!_nameofpadi.containsKey(dat2.getId())){
                _nameofpadi.put(dat2.getId(),dat2.getNama());
            }
        }

        Map<Integer, List<TranningData>> dataM = new HashMap<Integer, List<TranningData>>();
        List<Integer> keysa = new ArrayList<>();
        for (TranningData ds : homeModel.getDataTranning()) {
            List<TranningData> dsc = new ArrayList<>();
            if (!dataM.containsKey(ds.getKodepadi())) {
                dsc.add(ds);
                dataM.put(ds.getKodepadi(), dsc);
                keysa.add(ds.getKodepadi());
            } else {
                dataM.get(ds.getKodepadi()).add(ds);
            }
        }

        int jumlahTranning = homeModel.getDataTranning().size();
        int jumlahKriteria = homeModel.getDataKriteria().size();
        int jumlahVariable = homeModel.getDataVariable().size();
        int jumlahHimpunan = homeModel.getDataHimpunan().size();
        s = new String[jumlahKriteria];
        System.out.println("AM I here :" + jumlahTranning + " " + jumlahKriteria + " " + jumlahVariable + " " + jumlahHimpunan);


        for (int xas = 0; xas < keysa.size(); xas++) {
            List<TranningData> dataList = dataM.get(keysa.get(xas));
            System.out.println(dataList.toString());
            calculation(dataList, homeModel.getDataKriteria(), homeModel.getDataHimpunan(), homeModel.getDataVariable(), callbackProbabilitas);
        }

    }

    //TODO: Make this to a sync task
    private void calculation(List<TranningData> dataList, List<KriteriaData> dataKriteria, List<HimpunanData> dataHimpunan, List<VariableData> dataVariable, final HomeViewModel.CallbackProbabilitas callbackProbabilitas) {

        //for probabilitas
        Map<Integer, Map<Integer, Integer>> m = new HashMap<Integer, Map<Integer, Integer>>();
        List<Integer> keyMaps = new ArrayList<>();

        //probabilitas tiap variable
        //idvar,varhimpunan,jmlberhasil
        Map<Integer, Map<Integer, Integer>> pb = new HashMap<Integer, Map<Integer, Integer>>();
        Map<Integer, Map<Integer, Integer>> pt = new HashMap<Integer, Map<Integer, Integer>>();

        for (TranningData data : dataList) {

            String string = data.getSkodekriteria();
            System.out.println("Kriteria All : " + string);
            String[] parts = string.split(",");
            int[] pbi = new int[parts.length];
            String pbb = "";
            for (int x = 0; x < parts.length; x++) {
                System.out.println("Kriteria " + x + " : " + parts[x]);
                String w = parts[x].substring(1, 4);
                String v = parts[x].substring(4, 7);
                pbi[x] = Integer.parseInt(v);
                System.out.println("Kriteria : " + w + " - " + v);
                for (HimpunanData data2 : dataHimpunan) {
                    if (data2.getId() == Integer.parseInt(v)) {
                        System.out.println("This : " + Integer.parseInt(v) + " == " + data2.getId());
                        Map<Integer, Integer> n = new HashMap<Integer, Integer>();
                        if (!m.containsKey(Integer.parseInt(w))) {
                            System.out.println(!m.containsKey(w) + " new m Kay add " + w);
                            n.put(Integer.parseInt(v), 1);
                            m.put(Integer.parseInt(w), n);
                            if (!keyMaps.contains(Integer.parseInt(v))) {
                                keyMaps.add(Integer.parseInt(v));
                            }
                        } else {
                            System.out.println("Key " + w + " alredy");
                            n.putAll(m.get(Integer.parseInt(w)));
                            m.remove(Integer.parseInt(w));
                            if (!n.containsKey(Integer.parseInt(v))) {
                                System.out.println("Adding keyHash " + v);
                                n.put(Integer.parseInt(v), 1);
                            } else {
                                int i = n.get(Integer.parseInt(v));
                                System.out.println("Edit for value keyHash " + v);
                                n.remove(Integer.parseInt(v));
                                n.put(Integer.parseInt(v), (i + 1));
                            }
                            if (!keyMaps.contains(Integer.parseInt(v))) {
                                keyMaps.add(Integer.parseInt(v));
                            }
                            m.put(Integer.parseInt(w), n);
                        }
                        //
                        if (Integer.parseInt(w) == POINT_OF_CLASS[0] && Integer.parseInt(v) == POINT_OF_CLASS[2]) {
                            pbb = "berhasil";
                        }
                        if (Integer.parseInt(w) == POINT_OF_CLASS[0] && Integer.parseInt(v) == POINT_OF_CLASS[1]) {
                            pbb = "tidak berhasil";
                        }
                    }
                }
            }
            //tranning
            if (pbb.equals("berhasil")) {
                int xc = 0;
                for (VariableData data3 : variable) {
                    Map<Integer, Integer> hsd = new HashMap<Integer, Integer>();
                    if (!pb.containsKey(data3.getId())) {
                        System.out.println("new m Kay add PB " + data3.getId());
                        hsd.put(pbi[xc], 1);
                        pb.put(data3.getId(), hsd);
                    } else {
                        System.out.println("Key PB " + data3.getId() + " alredy");
                        hsd.putAll(pb.get(data3.getId()));
                        pb.remove(data3.getId());
                        if (!hsd.containsKey(pbi[xc])) {
                            System.out.println("Adding keyHash PB " + pbi[xc]);
                            hsd.put(pbi[xc], 1);
                        } else {
                            int i = hsd.get(pbi[xc]);
                            System.out.println("Edit for value keyHash PB " + pbi[xc]);
                            hsd.remove(pbi[xc]);
                            hsd.put(pbi[xc], (i + 1));
                        }
                        pb.put(data3.getId(), hsd);
                    }
                    xc++;
                }
            }
            if (pbb.equals("tidak berhasil")) {
                int xc = 0;
                for (VariableData data3 : variable) {
                    Map<Integer, Integer> hsd = new HashMap<Integer, Integer>();
                    if (!pt.containsKey(data3.getId())) {
                        System.out.println("new m Kay add PT " + data3.getId());
                        hsd.put(pbi[xc], 1);
                        pt.put(data3.getId(), hsd);
                    } else {
                        System.out.println("Key PT " + data3.getId() + " alredy");
                        hsd.putAll(pt.get(data3.getId()));
                        pt.remove(data3.getId());
                        if (!hsd.containsKey(pbi[xc])) {
                            System.out.println("Adding keyHash PT " + pbi[xc]);
                            hsd.put(pbi[xc], 1);
                        } else {
                            int i = hsd.get(pbi[xc]);
                            System.out.println("Edit for value keyHash PT " + pbi[xc]);
                            hsd.remove(pbi[xc]);
                            hsd.put(pbi[xc], (i + 1));
                        }
                        pt.put(data3.getId(), hsd);
                    }
                    xc++;
                }
            }
        }

        new CountDownTimer(5000, 1000) {

            public void onTick(long millisUntilFinished) {
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                List<Integer> keys = new ArrayList<>();

                for (TranningData data : dataList){
                    List<VariableModel> variableModels = new ArrayList<>();
                    System.out.println("KEY MAPS : " + keyMaps.toString());

                    for (VariableData data1 : dataVariable) {
                        List<HimpunanModel> himpunanModels = new ArrayList<>();
                        Map<Integer, Integer> n = new HashMap<Integer, Integer>();
                        Map<Integer, Integer> l = new HashMap<Integer, Integer>();
                        Map<Integer, Integer> i = new HashMap<Integer, Integer>();
                        int g = data1.getId();
                        n = m.get(g);
                        l = pb.get(g);
                        i = pt.get(g);
                        System.out.println(l.toString());
                        System.out.println(i.toString());
                        System.out.println("Panjang Himpunan : " + n.size());
                        for (int b = 0; b < keyMaps.size(); b++) {
                            int f = keyMaps.get(b);
                            if (n.containsKey(f) || l.containsKey(f)) {
                                int v = n.get(f) == null ? 0 : n.get(f);
                                int s = l.get(f) == null ? 0 : l.get(f);
                                int o = i.get(f) == null ? 0 : i.get(f);
//                    s[b] = "Variabel : " + g + " Himpunan " + f + " Nilai " + v;
                                System.out.println("Kode Padi " + data.getId() + " Variabel : " + g + " Himpunan " + f + " Nilai " + v + " Berhasil " + s + " Tidak Berhasil " + o);
                            himpunanModels.add(new HimpunanModel(f,String.valueOf(f), v, s, o));
                            }
                        }
                        variableModels.add(new VariableModel(data1.getId(), data1.getNama(), himpunanModels));
                    }
                    ProbabilitasItem item = new ProbabilitasItem(data.getKodepadi(), _nameofpadi.get(data.getKodepadi()), variableModels);
                    if (!keys.contains(item.getId())){
                        Log.d(TAG, "add "+data.getKodepadi());
                        probabilitasItems.add(item);
                        keys.add(item.getId());
                    }
                }
                callbackProbabilitas.onResult(probabilitasItems);
            }

        }.start();
    }

    public void insert(HimpunanData data) {
        homeRepository.insert(data);
    }

    public void listDetailsTranning(HomeModel homeModel, final HomeViewModel.CallbackTranningDetalis callbackTranningDetalis){
        for (TranningData data : homeModel.getDataTranning()){
            List<VHdataModel> vHdataModels = new ArrayList<>();
            String string = data.getSkodekriteria();
            System.out.println("Kriteria All : " + string);
            String[] parts = string.split(",");
            for (int x = 0; x < parts.length; x++) {
                String w = parts[x].substring(1, 4);
                String v = parts[x].substring(4, 7);
                Map<Integer,String> v1 = new HashMap<Integer, String>();
                List<Integer> keyv1 = new ArrayList<>();
                for (VariableData data1 : homeModel.getDataVariable()){
                    if (w.equals(String.valueOf(data1.getId()))){
                        v1.put(data1.getId(),data1.getNama());
                        keyv1.add(data1.getId());
                    }
                }

                for (HimpunanData data1 : homeModel.getDataHimpunan()){
                    if(v.equals(String.valueOf(data1.getId()))){
                        v1.put(data1.getId(),data1.getNama());
                        keyv1.add(data1.getId());
                    }
                }
                vHdataModels.add(new VHdataModel(Integer.parseInt(parts[x]),keyv1.get(0),v1.get(keyv1.get(0)),keyv1.get(1),v1.get(keyv1.get(1))));
            }

            for (PadiData data1 : homeModel.getDataPadi()){
                if (data.getKodepadi() == data1.getId()){
                    tranningItemDetails.add(new TranningDataModel(data.getId(),data1.getNama(),vHdataModels));
                }
            }
            callbackTranningDetalis.onResult(tranningItemDetails);
        }
    }
}
