package in.setone.appdm.ui.home;

import androidx.annotation.Nullable;

/**
 * Authentication result : success (user details) or error message.
 */
class HomeResult {
    @Nullable
    private HomeUiResult success;
    @Nullable
    private Integer error;

    HomeResult(@Nullable Integer error) {
        this.error = error;
    }

    HomeResult(@Nullable HomeUiResult success) {
        this.success = success;
    }

    @Nullable
    HomeUiResult getSuccess() {
        return success;
    }

    @Nullable
    Integer getError() {
        return error;
    }
}
