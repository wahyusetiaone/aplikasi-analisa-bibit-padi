package in.setone.appdm.ui.home.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import in.setone.appdm.R;
import in.setone.appdm.data.newtranning.HimpunanModel;
import in.setone.appdm.data.newtranning.VariableModel;

import java.util.List;

public class ProbabilitasAdapter extends RecyclerView.Adapter<ProbabilitasAdapter.ProbabilitasViewHolder> {

    private List<VariableModel> variableData;

    public ProbabilitasAdapter(List<VariableModel> variableData) {
        this.variableData = variableData;
    }

    @NonNull
    @Override
    public ProbabilitasAdapter.ProbabilitasViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recycler_view_item_list_home, parent, false);
        return new ProbabilitasViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProbabilitasAdapter.ProbabilitasViewHolder holder, int position) {
        holder.title.setText(variableData.get(position).getName());

        List<HimpunanModel> item = variableData.get(position).getHimpunan();

        holder.him1.setText(item.get(0).getNama());
        holder.him2.setText(item.get(1).getNama());
//        holder.jml1.setText(item.get(0).getJumlah());
//        holder.jml2.setText(item.get(1).getJumlah());
    }

    @Override
    public int getItemCount() {
        return (variableData != null) ? variableData.size() : 0;
    }

    public class ProbabilitasViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private TextView title,him1,him2,jml1,jml2;
        public ProbabilitasViewHolder(@NonNull View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.imageItem);
            title = (TextView) itemView.findViewById(R.id.tvTitlePadiHome);
            him1 = (TextView) itemView.findViewById(R.id.tvHimpunan1);
            him2 = (TextView) itemView.findViewById(R.id.tvVal1);
            jml1 = (TextView) itemView.findViewById(R.id.tvJmlHimpunan1);
            jml2 = (TextView) itemView.findViewById(R.id.tvJmlHimpunan2);
        }
    }
}
